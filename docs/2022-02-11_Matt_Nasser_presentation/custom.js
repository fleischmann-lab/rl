// select target target
const targetItem = document.querySelector('#table-of-contents h2');

// create a new element
const newItem = document.createElement('h2');
newItem.innerHTML = `Outline`;

// replace `targetItem` with `newItem`
targetItem.parentNode.replaceChild(newItem, targetItem);
