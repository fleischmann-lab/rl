---
title: "RL project updates and failures..."
# author: "Andrea Pierré"
format:
  revealjs:
    theme: default
incremental: true
from: markdown+emoji
slide-number: true
auto-play-media: true
css: custom.css
# include-after: |
#   <script type="text/javascript">
#     Reveal.on('ready', event => {
#       if (event.indexh === 0) {
#         document.querySelector("div.has-logo > img.slide-logo").style.display = "none";
#       }
#     });
#     Reveal.addEventListener('slidechanged', (event) => {
#       if (event.indexh === 0) {
#         Reveal.configure({ slideNumber: null });
#         document.querySelector("div.has-logo > img.slide-logo").style.display = "none";
#       }
#       if (event.indexh === 1) { 
#         Reveal.configure({ slideNumber: 'c' });
#         document.querySelector("div.has-logo > img.slide-logo").style.display = null;
#       }
#     });
#   </script>
---

## Outline{visibility="uncounted"}
- Visited ports behavior
- Failure to learn the task after bug correction
- Adding the odor info in the observation space
- Gridworld environment using the standard Gym library
- Next steps

## Visited ports behavior -- during the experiment
![](medias/three subplots_daily.png){fig-align="center"}

[&rarr; Get a visual sense of what happened during the experiment]{.fragment .fade-in}

## Visited ports behavior -- during the simulation
- Get a visual sense of what happened during the simulation
- Visually compare experimental behavior with simulated behavior
- Useful mean for debugging
  - Found one major bug in the `reset()` function :bug:

## Visited ports behavior -- during the simulation {.smaller visibility="uncounted"}
![](medias/visited_portd_simu.png){fig-align="center"}

## Failure to learn the task after bug correction :bug:
:::: {.columns}
::: {.column width="40%"}
::: {.fragment .fade-in}
![](medias/4 ports model.png)
:::
::::

::: {.column width="40%"}
::: {.fragment .fade-in}
![](medias/tabular-policy-4-distinct-ports.png)
:::
::::

::: {.column width="20%"}
::: {.fragment .fade-in}
[Reward hacking ?]{.r-fit-text}
{{< video https://youtu.be/tlOIHko8ySg >}}
:::
::::
::::

## Adding the odor info in the observation space
![](medias/mdp.svg){fig-align="center"}

## Adding the odor info in the observation space{visibility="uncounted"}
![](medias/mdp+odor.svg){fig-align="center"}

## Adding the odor info in the observation space{.smaller}
<!-- ``` -->
<!-- State/observation: -->

<!-- (Arena, NoOdor) -->
<!-- (NorthPort, NoOdor) -->
<!-- (Odor, OdorA) -->
<!-- (Arena, OdorA) -->
<!-- (WestPort, OdorA) -->
<!-- (EndOfEpisode, OdorA) -->
<!-- ``` -->
| Step # | State/observation       | Cumulated rewards |
|:------:|:------------------------|:-----------------:|
| 1      | `(Arena, NoOdor)`       | -1                |
| 2      | `(NorthPort, NoOdor)`   | -2                |
| 3      | `(Odor, OdorA)`         | -1                |
| 4      | `(Arena, OdorA)`        | -2                |
| 5      | `(WestPort, OdorA)`     | -3                |
| 6      | `(EndOfEpisode, OdorA)` | 7                 |
: Episode rollout 

## Adding the odor info in the observation space{.smaller visibility="uncounted" .scrollable}

| Actions\\Observations | `(Arena, NoOdor)` | `(Arena, OdorA)` | `(Arena, OdorB)` | `(NorthPort, NoOdor)` | `(NorthPort, OdorA)` | `(NorthPort, OdorB)` | `(SouthPort, NoOdor)` | `(SouthPort, OdorA)` | `(SouthPort, OdorB)` | `(EastPort, NoOdor)` | `(EastPort, OdorA)` | `(EastPort, OdorB)` | `(WestPort, NoOdor)` | `(WestPort, OdorA)` | `(WestPort, OdorB)` | `(Odor, OdorA)` | `(Odor, OdorB)` | `(EndOfEpisode, NoOdor)` | `(EndOfEpisode, OdorA)` | `(EndOfEpisode, OdorB)` |
|:----------------------|:-----------------:|:----------------:|:----------------:|:---------------------:|:--------------------:|:--------------------:|:---------------------:|:--------------------:|:--------------------:|:--------------------:|:-------------------:|:-------------------:|:--------------------:|:-------------------:|:-------------------:|:---------------:|:---------------:|:------------------------:|:-----------------------:|:-----------------------:|
| DoNothing             |                   |                  |                  |                       |                      |                      |                       |                      |                      |                      |                     |                     |                      |                     |                     |                 |                 |                          |                         |                         |
| MoveNorth             |                   |                  |                  |                       |                      |                      |                       |                      |                      |                      |                     |                     |                      |                     |                     |                 |                 |                          |                         |                         |
| MoveSouth             |                   |                  |                  |                       |                      |                      |                       |                      |                      |                      |                     |                     |                      |                     |                     |                 |                 |                          |                         |                         |
| MoveEast              |                   |                  |                  |                       |                      |                      |                       |                      |                      |                      |                     |                     |                      |                     |                     |                 |                 |                          |                         |                         |
| MoveWest              |                   |                  |                  |                       |                      |                      |                       |                      |                      |                      |                     |                     |                      |                     |                     |                 |                 |                          |                         |                         |
| MoveArena             |                   |                  |                  |                       |                      |                      |                       |                      |                      |                      |                     |                     |                      |                     |                     |                 |                 |                          |                         |                         |
| PokePlusSniff         |                   |                  |                  |                       |                      |                      |                       |                      |                      |                      |                     |                     |                      |                     |                     |                 |                 |                          |                         |                         |
| PokePlusLick          |                   |                  |                  |                       |                      |                      |                       |                      |                      |                      |                     |                     |                      |                     |                     |                 |                 |                          |                         |                         |
| Poke                  |                   |                  |                  |                       |                      |                      |                       |                      |                      |                      |                     |                     |                      |                     |                     |                 |                 |                          |                         |                         |
| Restart               |                   |                  |                  |                       |                      |                      |                       |                      |                      |                      |                     |                     |                      |                     |                     |                 |                 |                          |                         |                         |
: Q table

## Gridworld environment using the standard Gym library{.smaller}
:::: {.columns}
::: {.column width="40%"}
::: {.fragment .fade-in}
{{< video medias/rl-video-episode-0.mp4 width="400" height="300" loop="true">}}
:::
::::

::: {.column width="60%"}
::: {.fragment .fade-in}
- Uses the Gym library
  - Standard API for many RL environments
    - Ex. `step()` function:
    ```{.python .fragment .fade-in code-line-numbers="false"}
observation, reward, terminated, truncated, info = env.step(action)
    ```
    - Ex. `Spaces` classes &rarr; several predefined classes to describe the format of valid actions and observations
  - Leverages wrappers and helper functions
- Gridworld
  - Makes more sense to study spatial task
  - Allow to record and visually inspect what happened during an episode 
:::
::::
::::

## Next steps
- Hopefully make the odor observation work
- Finish the gridworld environment
  - Divide the experimental arena in tiles to be able to compare the experiment vs. the simulation ?
- Model Keeley's task?
- Replace the Q table by a simple neural network with hidden layers corresponding to 1) piriform, 2) LEC and 3) hippocampus
