# Example to make Vega tree collapsible
# - https://stackoverflow.com/a/73294259/4129062
# - https://stackoverflow.com/a/70438454/4129062

using DrWatson
@quickactivate "RL"

using Test
using JSON
using Suppressor
using LoggingExtras

# include(scriptsdir("DiamondArenaEnv.jl"))
include(srcdir("4_distinct_ports+odor_env.jl"))
MDP = get_mdp(:NorthPort, :pre, :OdorA)


function PlotMdpDataStructure(mdp::NamedTuple; filename::String = "", ext::String)

    # Remove output files
    rm(vegafilename(filename, "json"), force = true)
    rm(vegafilename(filename, ext), force = true)

    # Convert NamedTuple to expected row-oriented JSON format
    states = keys(mdp)
    actions = collect(Iterators.flatten([keys(mdp[state]) for state in keys(mdp)]))
    root_id = 1
    id = root_id
    data = [Dict("id" => id, "name" => "MDP")]
    for (s, state) in enumerate(keys(mdp))
        # Add states
        id += 1
        state_id = id
        push!(data, Dict("id" => state_id, "name" => state, "parent" => root_id))

        for (a, action) in enumerate(keys(mdp[state]))
            # Add actions
            id += 1
            action_id = id
            push!(data, Dict("id" => id, "name" => action, "parent" => state_id))


            for (p, prop) in enumerate(keys(mdp[state][action]))
                # Add properties
                id += 1
                push!(
                    data,
                    Dict(
                        "id" => id,
                        "name" => "$prop: $(mdp[state][action][prop])",
                        "parent" => action_id,
                    ),
                )
            end
        end
    end

    # Convert data to JSON
    data_json = JSON.json(data)
    print(data_json)

    # Write Vega spec to file
    vegastr = """
{
  "\$schema": "https://vega.github.io/schema/vega/v5.json",
  "description": "Data structure",
  "width": 600,
  "height": 1600,
  "padding": 5,
  "data": [
    {
      "name": "tree",
      "values": $data_json,
      "transform": [
        {
          "type": "stratify",
          "key": "id",
          "parentKey": "parent"
        },
        {
          "type": "tree",
          "method": "tidy",
          "size": [{"signal": "height"}, {"signal": "width"}],
          "separation": true,
          "as": ["y", "x", "depth", "children"]
        }
      ]
    },
    {
      "name": "links",
      "source": "tree",
      "transform": [
        { "type": "treelinks" },
        {
          "type": "linkpath",
          "orient": "horizontal",
          "shape": "orthogonal"
        }
      ]
    }
  ],

  "scales": [
    {
      "name": "color",
      "type": "linear",
      "range": {"scheme": "magma"},
      "domain": {"data": "tree", "field": "depth"},
      "zero": true
    }
  ],

  "marks": [
    {
      "type": "path",
      "from": {"data": "links"},
      "encode": {
        "update": {
          "path": {"field": "path"},
          "stroke": {"value": "#ccc"}
        }
      }
    },
    {
      "type": "symbol",
      "from": {"data": "tree"},
      "encode": {
        "enter": {
          "size": {"value": 500},
          "stroke": {"value": "#fff"}
        },
        "update": {
          "x": {"field": "x"},
          "y": {"field": "y"},
          "fill": {"scale": "color", "field": "depth"}
        }
      }
    },
    {
      "type": "text",
      "from": {"data": "tree"},
      "encode": {
        "enter": {
          "text": {"field": "name"},
          "fontSize": {"value": 23},
          "baseline": {"value": "middle"}
        },
        "update": {
          "x": {"field": "x"},
          "y": {"field": "y"},
          "dx": {"signal": "datum.children ? -15 : 15"},
          "align": {"signal": "datum.children ? 'right' : 'left'"}
        }
      }
    }
  ]
}
"""
    open(vegafilename(filename, "json"), "w") do f
        write(f, vegastr)
    end

    # Test if `vega-cli` is installed/working`
    try
        cmd = `vg2pdf --help`
        output = @capture_out begin
            Base.run(cmd)
        end
        @test occursin("Render a Vega specification to", output)
    catch error
        println(
            "Got an error running `vg2pdf`, please install it with `npm install -g vega-cli`",
        )
    end


    # Convert JSON
    vega_supported_formats = ["pdf", "png", "svg"]
    if ext ∈ vega_supported_formats
        cmd = `vg2$ext -s 2 $(vegafilename(filename, "json")) $(vegafilename(filename, ext))`
    else
        error("Unsupported extension $ext")

    end
    Base.run(cmd)
end


fn = plotsdir("mdp_data_structure")
ext = "pdf"
vegafilename(path::String, ext::String) = "$path.$ext"
PlotMdpDataStructure(MDP, filename = fn, ext = ext)
cmd = `xdg-open $(vegafilename(fn, ext))`
Base.run(cmd)
