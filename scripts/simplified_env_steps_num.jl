using DrWatson
@quickactivate "RL"

using Debugger
break_on(:error, :throw)

using Revise
using ReinforcementLearning
using Flux
using Statistics
using Random
using DataFrames
using CSV
using LoggingExtras

includet(srcdir("utils.jl"))
includet(srcdir("diamond_arena_simplified_env.jl"))
# includet(srcdir("CustomHooks.jl"))
includet(srcdir("plotting.jl"))
io, logger = logging(Logging.Debug)


n_runs = 50
n_episodes = 100
title = "simplified env steps number"
Random.seed!(42)


env, NS, NA = diamond_arena_simplified_env()

n_steps = zeros(n_runs, n_episodes)
for run_id = 1:n_runs
    learner = MonteCarloLearner(;
        approximator = TabularQApproximator(
            n_state = NS,
            n_action = NA,
            init = 0.0,
            opt = InvDecay(1.0),
        ),
        γ = 1.0,
    )
    explorer = EpsilonGreedyExplorer(
        kind = :linear,
        ϵ_init = 0.9,
        ϵ_stable = 0.1,
        warmup_steps = 100,
        decay_steps = 100,
    )
    policy = QBasedPolicy(learner = learner, explorer = explorer)
    agent = Agent(policy = policy, trajectory = VectorSARTTrajectory())

    # hook = TotalRewardPerEpisode(is_display_on_exit = true)
    hook = StepsPerEpisode()
    # hook = EpisodeBehavior()
    stop_condition = StopAfterEpisode(n_episodes, is_show_progress = true)

    run(agent, env, stop_condition, hook)
    n_steps[run_id, :] = hook.steps[1:n_episodes]
    display(agent.policy.learner.approximator.table)
end

df = DataFrame(episode = 1:n_episodes, steps = vec(mean(n_steps, dims = 1)))
lower, upper = zeros(n_episodes), zeros(n_episodes)
for ep = 1:n_episodes
    lower[ep], upper[ep] = confint(OneSampleTTest(n_steps[:, ep]))
end
@save datadir("sims", savename(Dict("title" => title), "bson")) df upper lower


close(io)
print("\nDone!")
