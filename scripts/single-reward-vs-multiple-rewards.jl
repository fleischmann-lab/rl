using DrWatson
@quickactivate "RL"

using Debugger
break_on(:error, :throw)

using ReinforcementLearning
using Flux
using LoggingExtras
using Dates
using Statistics
using Random
using AlgebraOfGraphics, CairoMakie
using DataFrames
using CategoricalArrays
using HypothesisTests


# Logging
const date_format = "yyyy-mm-dd HH:MM:SS"
logfile = "logs.txt"
io = open(logfile, "w+")
timestamp_logger(logger) =
    TransformerLogger(logger) do log
        merge(log, (; message = "$(Dates.format(now(), date_format)) - $(log.message)"))
    end
logger = SimpleLogger(io, Logging.Info) |> timestamp_logger


title = "Single reward vs. multiple rewards"
rewards = []
model = ["single", "multiple"]
model_cat = categorical(model)
allparams = Dict("model" => model)
dicts = dict_list(allparams)
n_runs = 30
n_episodes = 100


# Setup the environment
EndOfEpisode = "EndOfEpisode"
const StartOfEpisode = "Arena"
Base.@kwdef mutable struct DiamondArenaMdp <: AbstractEnv
    current_state::String = StartOfEpisode
    reward::Float64 = 0.0
end


function sim_mdp(model::String)

    if model == "single"
        DO_NOTHING_NEG_REWARD = 0.0
        NO_REWARD = 0.0
        SMALL_REWARD = 0.0
        BIG_REWARD = 10.0

        MDP = (
            Arena = (
                DoNothing = (next_state = "Arena", reward = DO_NOTHING_NEG_REWARD),
                MoveCue = (next_state = "CuePort", reward = NO_REWARD),
                MoveOther = (next_state = "OtherPort", reward = NO_REWARD),
                MoveArena = (next_state = "Arena", reward = DO_NOTHING_NEG_REWARD),
            ),
            CuePort = (
                DoNothing = (next_state = "CuePort", reward = DO_NOTHING_NEG_REWARD),
                PokePlusSniff = (next_state = "Odor", reward = SMALL_REWARD),
                MoveArena = (next_state = "Arena", reward = NO_REWARD),
            ),
            OtherPort = (
                DoNothing = (next_state = "OtherPort", reward = DO_NOTHING_NEG_REWARD),
                MoveCue = (next_state = "CuePort", reward = NO_REWARD),
                MoveArena = (next_state = "Arena", reward = DO_NOTHING_NEG_REWARD),
            ),
            Odor = (
                DoNothing = (next_state = "Odor", reward = DO_NOTHING_NEG_REWARD),
                MoveLeftWest = (next_state = "RewardPort", reward = NO_REWARD),
                MoveOther = (next_state = "OtherPortPost", reward = NO_REWARD),
                MoveArena = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
            ),
            RewardPort = (
                DoNothing = (next_state = "RewardPort", reward = DO_NOTHING_NEG_REWARD),
                PokePlusLick = (next_state = EndOfEpisode, reward = BIG_REWARD),
                MoveArena = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
            ),
            ArenaPost = (
                DoNothing = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
                MoveLeftWest = (next_state = "RewardPort", reward = NO_REWARD),
                MoveOther = (next_state = "OtherPortPost", reward = NO_REWARD),
            ),
            OtherPortPost = (
                DoNothing = (next_state = "OtherPortPost", reward = DO_NOTHING_NEG_REWARD),
                Poke = (next_state = EndOfEpisode, reward = NO_REWARD),
                MoveArena = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
            ),
            EndOfEpisode = (Restart = (next_state = StartOfEpisode, reward = NO_REWARD),),
        )
    else

        DO_NOTHING_NEG_REWARD = -1.0
        NO_REWARD = 0.0
        SMALL_REWARD = 1.0
        BIG_REWARD = 10.0

        MDP = (
            Arena = (
                DoNothing = (next_state = "Arena", reward = DO_NOTHING_NEG_REWARD),
                MoveCue = (next_state = "CuePort", reward = NO_REWARD),
                MoveOther = (next_state = "OtherPort", reward = NO_REWARD),
                MoveArena = (next_state = "Arena", reward = DO_NOTHING_NEG_REWARD),
            ),
            CuePort = (
                DoNothing = (next_state = "CuePort", reward = DO_NOTHING_NEG_REWARD),
                PokePlusSniff = (next_state = "Odor", reward = SMALL_REWARD),
                MoveArena = (next_state = "Arena", reward = NO_REWARD),
            ),
            OtherPort = (
                DoNothing = (next_state = "OtherPort", reward = DO_NOTHING_NEG_REWARD),
                MoveCue = (next_state = "CuePort", reward = NO_REWARD),
                MoveArena = (next_state = "Arena", reward = DO_NOTHING_NEG_REWARD),
            ),
            Odor = (
                DoNothing = (next_state = "Odor", reward = DO_NOTHING_NEG_REWARD),
                MoveLeftWest = (next_state = "RewardPort", reward = NO_REWARD),
                MoveOther = (next_state = "OtherPortPost", reward = NO_REWARD),
                MoveArena = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
            ),
            RewardPort = (
                DoNothing = (next_state = "RewardPort", reward = DO_NOTHING_NEG_REWARD),
                PokePlusLick = (next_state = EndOfEpisode, reward = BIG_REWARD),
                MoveArena = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
            ),
            ArenaPost = (
                DoNothing = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
                MoveLeftWest = (next_state = "RewardPort", reward = NO_REWARD),
                MoveOther = (next_state = "OtherPortPost", reward = NO_REWARD),
            ),
            OtherPortPost = (
                DoNothing = (next_state = "OtherPortPost", reward = DO_NOTHING_NEG_REWARD),
                Poke = (next_state = EndOfEpisode, reward = -BIG_REWARD),
                MoveArena = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
            ),
            EndOfEpisode = (Restart = (next_state = StartOfEpisode, reward = NO_REWARD),),
        )
    end

    # Minimal Interfaces to Implement
    action_space(env::DiamondArenaMdp) = [
        string(item) for item in
        unique(collect(Iterators.flatten([keys(MDP[state]) for state in keys(MDP)])))
    ]
    reward(env::DiamondArenaMdp) = env.reward
    state(env::DiamondArenaMdp) = env.current_state
    state_space(env::DiamondArenaMdp) = [string(item) for item in keys(MDP)]
    is_terminated(env::DiamondArenaMdp) = state(env) == EndOfEpisode
    function reset!(env::DiamondArenaMdp)
        env.current_state = StartOfEpisode
        env.reward = 0.0
        nothing
    end
    ChanceStyle(env::DiamondArenaMdp) = DETERMINISTIC
    ActionStyle(env::DiamondArenaMdp) = FULL_ACTION_SET
    legal_action_space(env::DiamondArenaMdp) =
        action_space(env)[legal_action_space_mask(env)]
    function legal_action_space_mask(env::DiamondArenaMdp)
        mask = fill(false, size(action_space(env)))
        allowed_actions = [string(item) for item in keys(MDP[Symbol(state(env))])]
        for action ∈ allowed_actions
            mask = mask .|| (action_space(env) .== action)
        end
        mask
    end
    "Environment logic"
    function (env::DiamondArenaMdp)(action::String)
        original_state = state(env)  # Needs to store the state otherwise it gets overwriten
        env.current_state = MDP[Symbol(original_state)][Symbol(action)][:next_state]
        env.reward += MDP[Symbol(original_state)][Symbol(action)][:reward]
        with_logger(logger) do
            @debug("Original state: $original_state")
            @debug("Action: $action")
            @debug("New state: $(env.current_state)")
            @debug("Reward: $(env.reward)")
        end
        nothing
    end

    DiamondArenaEnv = DiamondArenaMdp()
    # RLBase.test_runnable!(DiamondArenaMdp())

    DiamondArenaWrapped = ActionTransformedEnv(
        StateTransformedEnv(
            DiamondArenaEnv;
            state_mapping = s -> begin
                res = findfirst(x -> x == s, state_space(DiamondArenaEnv))
                with_logger(logger) do
                    @logmsg Logging.Debug "Space mapping" s res
                end
                res
            end,
            state_space_mapping = _ -> Base.OneTo(length(state_space(DiamondArenaEnv))),
        );
        action_mapping = a -> begin
            res = action_space(DiamondArenaEnv)[a]
            with_logger(logger) do
                @logmsg Logging.Debug "Action mapping" a res
            end
            res
        end,
        action_space_mapping = _ -> Base.OneTo(length(action_space(DiamondArenaEnv))),
    )

    # Useful?
    # using MLBase
    # lm = labelmap(["a", "a", "b", "b", "c"])
    # labelencode(lm, ["a", "c", "b"])

    # RLBase.test_runnable!(DiamondArenaWrapped)

    NS, NA = length(state_space(DiamondArenaEnv)), length(action_space(DiamondArenaEnv))
    # policy = RandomPolicy()
    # policy = VBasedPolicy(
    #     learner = MonteCarloLearner(;
    #         approximator = TabularVApproximator(; n_state = NS, opt = InvDecay(1.0)),
    #         γ = 1.0,
    #         kind = FIRST_VISIT,
    #         sampling = NO_SAMPLING,
    #     ),
    # )
    # policy = QBasedPolicy(
    #     learner = MonteCarloLearner(;
    #         approximator = TabularQApproximator(;
    #             n_state = NS,
    #             n_action = NA,
    #             init = 0.0,
    #             opt = InvDecay(1.0),
    #         ),
    #         γ = 1.0,
    #         kind = FIRST_VISIT,
    #         sampling = NO_SAMPLING,
    #     ),
    #     explorer = EpsilonGreedyExplorer(0.1),
    # )
    # agent = Agent(
    #     policy = VBasedPolicy(
    #         learner = MonteCarloLearner(;
    #             approximator = TabularVApproximator(; n_state = NS, opt = InvDecay(1.0)),
    #             γ = 1.0,
    #             kind = FIRST_VISIT,
    #             sampling = NO_SAMPLING,
    #         ),
    #     ),
    #     trajectory = VectorSARTTrajectory(),
    # )

    # learner = MonteCarloLearner(;
    #     approximator = TabularQApproximator(;
    #         n_state = NS,
    #         n_action = NA,
    #         opt = InvDecay(1.0),
    #         # opt = Descent(1.0),
    #     ),
    #     γ = 0.2,
    #     kind = FIRST_VISIT,
    #     sampling = NO_SAMPLING,
    # )

    rewards = zeros(n_runs, n_episodes)

    for rew = 1:n_runs
        learner = TDLearner(
            approximator = TabularQApproximator(
                n_state = NS,
                n_action = NA,
                init = 0.0,
                opt = InvDecay(1.0),
            ),
            γ = 0.9,
            method = :ExpectedSARSA,
            # method = :SARSA,
            # method = :SARS,
            n = 0,
        )
        explorer = EpsilonGreedyExplorer(
            kind = :linear,
            ϵ_init = 0.9,
            ϵ_stable = 0.1,
            warmup_steps = 100,
            decay_steps = 100,
        )
        policy = QBasedPolicy(learner = learner, explorer = explorer)
        agent = Agent(policy = policy, trajectory = VectorSARTTrajectory())

        hook = TotalRewardPerEpisode(is_display_on_exit = true)
        # hook = RewardsPerEpisode()
        # stop_condition = StopAfterStep(100; is_show_progress = true)
        stop_condition = StopAfterEpisode(n_episodes, is_show_progress = true)
        # run(policy, DiamondArenaWrapped, stop_condition, hook)

        run(agent, DiamondArenaWrapped, stop_condition, hook)
        rewards[rew, :] = hook.rewards
        display(agent.policy.learner.approximator.table)
    end

    return rewards
end

function makesim(d::Dict)
    @unpack model = d
    rewards = sim_mdp(model)
    # fulld = copy(d)
    # fulld["rewards"] = rewards
    # return fulld
    return rewards
end

function res_prep()
    df_full = DataFrame(
        episode = Int[],
        reward = Float64[],
        model = CategoricalValue{String,UInt32}[],
        run_nb = Int[],
    )
    lower, upper = zeros(length(dicts), n_episodes), zeros(length(dicts), n_episodes)
    for (i, d) in enumerate(dicts)
        rewards = makesim(d)  # Simulate model

        for idx = 1:n_runs
            rew = rewards[idx, :]
            ep = collect(1:n_episodes)
            model = fill(model_cat[i], n_episodes)
            run_nb = fill(idx, n_episodes)
            for jdx = 1:n_episodes
                push!(df_full, (ep[jdx], rew[jdx], model[jdx], run_nb[jdx]))
            end
        end
        for ep = 1:n_episodes
            lower[i, ep], upper[i, ep] = confint(OneSampleTTest(rewards[:, ep]))
        end
    end
    df_avg = DataFrame(
        episode = Int[],
        reward_avg = Float64[],
        model = CategoricalValue{String,UInt32}[],
    )
    for (i, d) in enumerate(dicts)
        rew = zeros(n_episodes)
        for jdx = 1:n_episodes
            rew[jdx] =
                mean(df_full.reward[df_full.episode.==jdx.&&df_full.model.==d["model"], :])
        end
        ep = collect(1:n_episodes)
        model = fill(model_cat[i], n_episodes)
        for jdx = 1:n_episodes
            push!(df_avg, (ep[jdx], rew[jdx], model[jdx]))
        end
    end
    return df_full, df_avg, lower, upper
end

function plotting()
    df_full, df_avg, lower, upper = res_prep()
    # plt =
    #     data(df_avg) *
    #     mapping(
    #         :episode => "Episode",
    #         :reward_avg => "Cummulated rewards",
    #         color = :model => "model",
    #     ) *
    #     visual(Lines)
    # fg = draw(plt; axis = (; title = title), figure = (; backgroundcolor = :transparent))
    fg = Figure(; backgroundcolor = :transparent)
    Axis(
        fg[1, 1],
        title = title,
        xlabel = "Episode",
        ylabel = "Total rewards averaged over $n_runs runs",
    )
    cmap = [
        "#000000",
        "#E69F00",
        "#56B4E9",
        "#009E73",
        "#F0E442",
        "#0072B2",
        "#D55E00",
        "#CC79A7",
    ]
    for (i, d) in enumerate(dicts)
        linesfill!(
            collect(1:n_episodes),
            vec(df_avg.reward_avg[df_avg.model.==d["model"], :]);
            lower = lower[i, :],
            upper = upper[i, :],
            color = cmap[i],
            label = "model = $(d["model"])",
        )  # plot confidence interval band
    end
    axislegend(position = :rb)
    return fg
end

fn = plotsdir(title)
save("$fn.png", with_theme(plotting, theme_ggplot2()), px_per_unit = 3) # save high-resolution png
cmd = `xdg-open "$fn.png"`
Base.run(cmd)
# display(p)


# flush(io)
close(io)
