using DrWatson
@quickactivate "RL"

using Revise
using Debugger
break_on(:error, :throw)

using ReinforcementLearning
using Flux
using Statistics
using Random
using CairoMakie

includet(srcdir("utils.jl"))
# includet(srcdir("diamond_arena_simplified_env.jl"))
includet(srcdir("4_distinct_ports_env.jl"))
includet(srcdir("plotting.jl"))
io, logger = logging(Logging.Debug)


title = "tabular-policy-4-distinct-ports"
n_episodes = 200
Random.seed!(1234)


# env, NS, NA = diamond_arena_simplified_env()
env, NS, NA = four_distinct_ports_env()

learner = TDLearner(
    approximator = TabularQApproximator(
        n_state = NS,
        n_action = NA,
        init = 0.0,
        opt = InvDecay(1.0),
    ),
    γ = 1.0,
    method = :ExpectedSARSA,
    # method = :SARSA,
    # method = :SARS,
    n = 0,
)
explorer = EpsilonGreedyExplorer(
    kind = :linear,
    ϵ_init = 0.9,
    ϵ_stable = 0.1,
    warmup_steps = 100,
    decay_steps = 100,
)
policy = QBasedPolicy(learner = learner, explorer = explorer)
agent = Agent(policy = policy, trajectory = VectorSARTTrajectory())

hook = TotalRewardPerEpisode(is_display_on_exit = true)
# hook = RewardsPerEpisode()
# stop_condition = StopAfterStep(50; is_show_progress = true)
# n_runs = 3
stop_condition = StopAfterEpisode(n_episodes, is_show_progress = true)
# run(policy, env, stop_condition, hook)

# rewards = zeros(n_runs, n_episodes)
# for rew = 1:n_runs
run(agent, env, stop_condition, hook)
#     rewards[rew, :] = hook.rewards
# end

display(agent.policy.learner.approximator.table)
create_plot(plot_policy, title, true)

close(io)
print("\nDone!")
