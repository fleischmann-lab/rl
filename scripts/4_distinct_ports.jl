using DrWatson
@quickactivate "RL"

using Debugger
break_on(:error, :throw)

using Revise
using ReinforcementLearning
using Flux
using Statistics
using Random
using DataFrames
using CSV
using LoggingExtras

includet(srcdir("utils.jl"))
includet(srcdir("4_distinct_ports_env.jl"))
includet(srcdir("CustomHooks.jl"))
includet(srcdir("plotting.jl"))
io, logger = logging(Logging.Debug)


n_runs = 100
n_episodes = 100
title = "4 ports model"
# title = "Port poking behavior"
Random.seed!(42)


env, NS, NA = four_distinct_ports_env()

rewards = zeros(n_runs, n_episodes)
for rew = 1:n_runs
    learner = MonteCarloLearner(;
        approximator = TabularQApproximator(
            n_state = NS,
            n_action = NA,
            init = 0.0,
            opt = InvDecay(1.0),
        ),
        γ = 1.0,
    )
    explorer = EpsilonGreedyExplorer(
        kind = :linear,
        ϵ_init = 0.9,
        ϵ_stable = 0.1,
        warmup_steps = 100,
        decay_steps = 100,
    )
    policy = QBasedPolicy(learner = learner, explorer = explorer)
    agent = Agent(policy = policy, trajectory = VectorSARTTrajectory())

    hook = TotalRewardPerEpisode(is_display_on_exit = true)
    # hook = EpisodeBehavior()
    # hook = RewardsPerEpisode()
    stop_condition = StopAfterEpisode(n_episodes, is_show_progress = true)

    run(agent, env, stop_condition, hook)
    rewards[:, run_i] = hook.rewards
    # display(agent.policy.learner.approximator.table)
end

df = DataFrame(episode = 1:n_episodes, reward = vec(mean(rewards, dims = 2)))
create_plot(
    plot_avg_cum_rewards;
    title = title,
    open_plot = true,
    n_runs = n_runs,
    n_episodes = n_episodes,
    df = df,
    rewards = rewards,
)

# h = DataFrame(
#     time = hook.timestamps,
#     N = hook.visitN,
#     S = hook.visitS,
#     E = hook.visitE,
#     W = hook.visitW,
#     Re = hook.rewards,
# )
# CSV.write(datadir("sims", "hook.csv"), h)

close(io)
print("\nDone!")
