using DrWatson
@quickactivate "RL"

using BSON: @load
using DataFrames
using CategoricalArrays
using AlgebraOfGraphics, CairoMakie


n_runs = 30

title = "Single reward only"
@load datadir("sims", savename(Dict("title" => title), "bson")) df_full df_avg lower upper
title = "Total-reward-per-episode"
lower_avg = lower
upper_avg = upper
@load datadir("sims", savename(Dict("title" => title), "bson")) df upper lower
n_episodes = length(df.episode)

title = "Single reward vs. multiple rewards"
function plotting()
    fg = Figure(; backgroundcolor = :transparent)
    Axis(
        fg[1, 1],
        title = title,
        xlabel = "Episode",
        ylabel = "Total reward averaged over $n_runs runs",
    )
    cmap = [
        "#000000",
        "#E69F00",
        "#56B4E9",
        "#009E73",
        "#F0E442",
        "#0072B2",
        "#D55E00",
        "#CC79A7",
    ]
    linesfill!(
        collect(1:n_episodes),
        vec(df_avg.reward_avg[df_avg.γ.==1.0, :]);
        lower = lower_avg[end, :],
        upper = upper_avg[end, :],
        color = cmap[1],
        label = "single reward",
    )  # plot confidence interval band
    linesfill!(
        collect(1:n_episodes),
        vec(df.reward);
        lower = lower,
        upper = upper,
        color = cmap[2],
        label = "multiple rewards",
    )  # plot confidence interval band
    axislegend(position = :rb)
    return fg
end

fn = plotsdir(title)
save("$fn.png", with_theme(plotting, theme_ggplot2()), px_per_unit = 3) # save high-resolution png
cmd = `xdg-open "$fn.png"`
Base.run(cmd)
