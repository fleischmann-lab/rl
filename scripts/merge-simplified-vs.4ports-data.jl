using DrWatson
@quickactivate "RL"

using BSON: @load
using DataFrames
using CategoricalArrays
using AlgebraOfGraphics, CairoMakie


n_runs = 30

# title = "4 ports model"
title = "4 ports steps number"
@load datadir("sims", savename(Dict("title" => title), "bson")) df lower upper
df4 = df
# title = "Total-reward-per-episode"
title = "simplified env steps number"
lower4 = lower
upper4 = upper
@load datadir("sims", savename(Dict("title" => title), "bson")) df upper lower
n_episodes = length(df.episode)

title = "Simplified vs. 4 ports environments"
function plotting()
    fg = Figure(; backgroundcolor = :transparent)
    Axis(
        fg[1, 1],
        title = title,
        xlabel = "Episode",
        # ylabel = "Total reward averaged over $n_runs runs",
        ylabel = "Total steps number averaged over $n_runs runs",
    )
    cmap = [
        "#000000",
        "#E69F00",
        "#56B4E9",
        "#009E73",
        "#F0E442",
        "#0072B2",
        "#D55E00",
        "#CC79A7",
    ]
    linesfill!(
        collect(1:n_episodes),
        # vec(df4.reward);
        vec(df4.steps);
        lower = lower4,
        upper = upper4,
        color = cmap[3],
        label = "4 ports",
    )  # plot confidence interval band
    linesfill!(
        collect(1:n_episodes),
        # vec(df.reward);
        vec(df.steps);
        lower = lower,
        upper = upper,
        color = cmap[8],
        label = "simplified",
    )  # plot confidence interval band
    axislegend(position = :rt)
    return fg
end

fn = plotsdir(title)
save("$fn.png", with_theme(plotting, theme_ggplot2()), px_per_unit = 3) # save high-resolution png
cmd = `xdg-open "$fn.png"`
Base.run(cmd)
