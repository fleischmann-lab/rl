using DrWatson
@quickactivate "RL"

using TikzPictures


DO_NOTHING_NEG_REWARD = -1.0
const NO_REWARD = 0.0
const EndOfEpisode = "EndOfEpisode"
const StartOfEpisode = "Arena"
SMALL_REWARD = 1.0
BIG_REWARD = 10.0
MDP = (
    Arena = (
        DoNothing = (next_state = "Arena", reward = DO_NOTHING_NEG_REWARD),
        MoveCue = (next_state = "CuePort", reward = NO_REWARD),
        MoveOther = (next_state = "OtherPort", reward = NO_REWARD),
        MoveArena = (next_state = "Arena", reward = DO_NOTHING_NEG_REWARD),
    ),
    CuePort = (
        DoNothing = (next_state = "CuePort", reward = DO_NOTHING_NEG_REWARD),
        PokePlusSniff = (next_state = "Odor", reward = SMALL_REWARD),
        MoveArena = (next_state = "Arena", reward = NO_REWARD),
    ),
    OtherPort = (
        DoNothing = (next_state = "OtherPort", reward = DO_NOTHING_NEG_REWARD),
        MoveCue = (next_state = "CuePort", reward = NO_REWARD),
        MoveArena = (next_state = "Arena", reward = DO_NOTHING_NEG_REWARD),
    ),
    Odor = (
        DoNothing = (next_state = "Odor", reward = DO_NOTHING_NEG_REWARD),
        MoveLeftWest = (next_state = "RewardPort", reward = NO_REWARD),
        MoveOther = (next_state = "OtherPortPost", reward = NO_REWARD),
        MoveArena = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
    ),
    RewardPort = (
        DoNothing = (next_state = "RewardPort", reward = DO_NOTHING_NEG_REWARD),
        PokePlusLick = (next_state = EndOfEpisode, reward = BIG_REWARD),
        MoveArena = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
    ),
    ArenaPost = (
        DoNothing = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
        MoveLeftWest = (next_state = "RewardPort", reward = NO_REWARD),
        MoveOther = (next_state = "OtherPortPost", reward = NO_REWARD),
    ),
    OtherPortPost = (
        DoNothing = (next_state = "OtherPortPost", reward = DO_NOTHING_NEG_REWARD),
        Poke = (next_state = EndOfEpisode, reward = -BIG_REWARD),
        MoveArena = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
    ),
    EndOfEpisode = (Restart = (next_state = StartOfEpisode, reward = NO_REWARD),),
)

function PlotMDP(mdp::NamedTuple; filename::String = "", saveas::Symbol = :PDF)

    tikzstr = ["\\node {MDP} \n"]
    for state in keys(mdp)
        # Add each state node
        push!(tikzstr, "  child { node [state] {$state}\n")

        for action in keys(mdp[state])
            # Add each action node
            push!(tikzstr, "    child { node [action] {$action}\n")

            for prop in keys(mdp[state][action])
                # Add each property node
                push!(
                    tikzstr,
                    "      child { node [prop] {$(replace(String(prop), "_" => " ")) : " *
                    "$(mdp[state][action][prop])}}\n",
                )
            end
            push!(tikzstr, "}\n")  # Dont't forget to close the child
            for prop in mdp[state][action]
                # Need to add some fake nodes to avoid overlapping nodes
                push!(tikzstr, "  child [missing] {}\n")
            end
        end
        push!(tikzstr, "}\n")  # Dont't forget to close the child
        for action in keys(mdp[state])
            # Need to add some fake nodes to avoid overlapping nodes
            push!(tikzstr, "  child [missing] {}\n")
            for prop in mdp[state][action]
                # Need to add some fake nodes to avoid overlapping nodes
                push!(tikzstr, "  child [missing] {}\n")
            end
        end
    end
    push!(tikzstr, ";")
    tp = TikzPicture(
        join(tikzstr),
        options = "grow via three points={one child at (0.5,-0.7) and
  two children at (0.5,-0.7) and (0.5,-1.4)},
  edge from parent path={(\\tikzparentnode.south) |- (\\tikzchildnode.west)}",
        preamble = """
\\usetikzlibrary{trees}
\\tikzstyle{every node}=[draw=black,thick,anchor=west, rounded corners=0.1cm]
\\tikzstyle{state}=[fill=blue!10]
\\tikzstyle{action}=[fill=green!10]
\\tikzstyle{prop}=[fill=yellow!10]
""",
    )

    # Save to file
    save_allowed = (:PDF, :SVG, :TEX)
    if !isempty(filename)
        if saveas == :PDF
            TikzPictures.save(PDF(filename), tp)
        elseif saveas == :SVG
            TikzPictures.save(SVG(filename), tp)
        elseif saveas == :TEX
            TikzPictures.save(TEX(filename), tp)
        else
            throw(ArgumentError("`saveas` argument can only be $save_allowed"))
        end
    end
    return join(tikzstr)
end

fn = plotsdir("mdp_data_structure")
saveas = :PDF
p = PlotMDP(MDP, filename = fn, saveas = saveas)
cmd = `xdg-open "$(fn).$(String(saveas) |> lowercase)"`
Base.run(cmd)
