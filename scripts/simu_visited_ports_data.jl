using DrWatson
@quickactivate "RL"

using Debugger
break_on(:error, :throw)

using Revise
using ReinforcementLearning
using Flux
using Statistics
using Random
using DataFrames
using CSV
using LoggingExtras

includet(srcdir("utils.jl"))
includet(srcdir("4_distinct_ports_env.jl"))
includet(srcdir("CustomHooks.jl"))
includet(srcdir("plotting.jl"))
io, logger = logging(Logging.Debug)


n_episodes = 30
title = "Visited ports"
Random.seed!(1234)


env, NS, NA = four_distinct_ports_env()

learner = MonteCarloLearner(;
    approximator = TabularQApproximator(
        n_state = NS,
        n_action = NA,
        init = 0.0,
        opt = InvDecay(1.0),
    ),
    γ = 1.0,
)
explorer = EpsilonGreedyExplorer(
    kind = :linear,
    ϵ_init = 0.9,
    ϵ_stable = 0.1,
    warmup_steps = 100,
    decay_steps = 100,
)
policy = QBasedPolicy(learner = learner, explorer = explorer)
agent = Agent(policy = policy, trajectory = VectorSARTTrajectory())
hook = EpisodeBehavior()
stop_condition = StopAfterEpisode(n_episodes, is_show_progress = true)
run(agent, env, stop_condition, hook)
display(agent.policy.learner.approximator.table)

h = DataFrame(
    steps = hook.steps,
    N = hook.visitN,
    S = hook.visitS,
    E = hook.visitE,
    W = hook.visitW,
    Re = hook.rewards,
    start = hook.startEpisode,
    stop = hook.endEpisode,
)
CSV.write(
    datadir("sims", savename(Dict("title" => title, "n_episodes" => n_episodes), "csv")),
    h,
)

close(io)
print("\nDone!")
