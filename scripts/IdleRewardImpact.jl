using DrWatson
@quickactivate "RL"

using Debugger
break_on(:error, :throw)

using ReinforcementLearning
using Flux
using LoggingExtras
using Dates
using Statistics
# using Plots
# theme(:ggplot2)
using Random
using AlgebraOfGraphics, CairoMakie
# set_aog_theme!()
# using Latexify
using DataFrames
using CategoricalArrays


# Logging
const date_format = "yyyy-mm-dd HH:MM:SS"
logfile = "logs.txt"
io = open(logfile, "w+")
timestamp_logger(logger) =
    TransformerLogger(logger) do log
        merge(log, (; message = "$(Dates.format(now(), date_format)) - $(log.message)"))
    end
logger = SimpleLogger(io, Logging.Info) |> timestamp_logger


# Setup the environment
Random.seed!(1234)
StartOfEpisode = "Arena"
Base.@kwdef mutable struct DiamondArenaMdp <: AbstractEnv
    current_state::String = StartOfEpisode
    reward::Float64 = 0.0
end

rewards = []
do_nothing_reward = [0.0, -1.0, -10.0]
title = "Punishment impact"
# p = plot(title=title)
n_episodes = 200
for (i, DO_NOTHING_NEG_REWARD) ∈ enumerate(do_nothing_reward)
    # DO_NOTHING_NEG_REWARD = -1.0
    NO_REWARD = 0.0
    EndOfEpisode = "EndOfEpisode"
    SMALL_REWARD = 1.0
    BIG_REWARD = 10.0


    MDP = (
        Arena = (
            DoNothing = (next_state = "Arena", reward = DO_NOTHING_NEG_REWARD),
            MoveCue = (next_state = "CuePort", reward = NO_REWARD),
            MoveOther = (next_state = "OtherPort", reward = NO_REWARD),
            MoveArena = (next_state = "Arena", reward = DO_NOTHING_NEG_REWARD),
        ),
        CuePort = (
            DoNothing = (next_state = "CuePort", reward = DO_NOTHING_NEG_REWARD),
            PokePlusSniff = (next_state = "Odor", reward = SMALL_REWARD),
            MoveArena = (next_state = "Arena", reward = NO_REWARD),
        ),
        OtherPort = (
            DoNothing = (next_state = "OtherPort", reward = DO_NOTHING_NEG_REWARD),
            MoveCue = (next_state = "CuePort", reward = NO_REWARD),
            MoveArena = (next_state = "Arena", reward = DO_NOTHING_NEG_REWARD),
        ),
        Odor = (
            DoNothing = (next_state = "Odor", reward = DO_NOTHING_NEG_REWARD),
            MoveLeftWest = (next_state = "RewardPort", reward = NO_REWARD),
            MoveOther = (next_state = "OtherPortPost", reward = NO_REWARD),
            MoveArena = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
        ),
        RewardPort = (
            DoNothing = (next_state = "RewardPort", reward = DO_NOTHING_NEG_REWARD),
            PokePlusLick = (next_state = EndOfEpisode, reward = BIG_REWARD),
            MoveArena = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
        ),
        ArenaPost = (
            DoNothing = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
            MoveLeftWest = (next_state = "RewardPort", reward = NO_REWARD),
            MoveOther = (next_state = "OtherPortPost", reward = NO_REWARD),
        ),
        OtherPortPost = (
            DoNothing = (next_state = "OtherPortPost", reward = DO_NOTHING_NEG_REWARD),
            Poke = (next_state = EndOfEpisode, reward = -BIG_REWARD),
            MoveArena = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
        ),
        EndOfEpisode = (Restart = (next_state = StartOfEpisode, reward = NO_REWARD),),
    )

    # Minimal Interfaces to Implement
    RLBase.action_space(env::DiamondArenaMdp) = [
        string(item) for item in
        unique(collect(Iterators.flatten([keys(MDP[state]) for state in keys(MDP)])))
    ]
    RLBase.reward(env::DiamondArenaMdp) = env.reward
    RLBase.state(env::DiamondArenaMdp) = env.current_state
    RLBase.state_space(env::DiamondArenaMdp) = [string(item) for item in keys(MDP)]
    RLBase.is_terminated(env::DiamondArenaMdp) = state(env) == EndOfEpisode
    function RLBase.reset!(env::DiamondArenaMdp)
        env.current_state = StartOfEpisode
        env.reward = 0.0
        nothing
    end
    RLBase.ChanceStyle(env::DiamondArenaMdp) = RLBase.DETERMINISTIC
    RLBase.ActionStyle(env::DiamondArenaMdp) = FULL_ACTION_SET
    RLBase.legal_action_space(env::DiamondArenaMdp) =
        RLBase.action_space(env)[legal_action_space_mask(env)]
    function RLBase.legal_action_space_mask(env::DiamondArenaMdp)
        mask = fill(false, size(RLBase.action_space(env)))
        allowed_actions = [string(item) for item in keys(MDP[Symbol(state(env))])]
        for action ∈ allowed_actions
            mask = mask .|| (RLBase.action_space(env) .== action)
        end
        mask
    end

    "Environment logic"
    function (env::DiamondArenaMdp)(action::String)
        original_state = state(env)  # Needs to store the state otherwise it gets overwriten
        env.current_state = MDP[Symbol(original_state)][Symbol(action)][:next_state]
        env.reward += MDP[Symbol(original_state)][Symbol(action)][:reward]
        with_logger(logger) do
            @debug("Original state: $original_state")
            @debug("Action: $action")
            @debug("New state: $(env.current_state)")
            @debug("Reward: $(env.reward)")
        end
        nothing
    end

    DiamondArenaEnv = DiamondArenaMdp()
    # RLBase.test_runnable!(DiamondArenaMdp())

    DiamondArenaWrapped = ActionTransformedEnv(
        StateTransformedEnv(
            DiamondArenaEnv;
            state_mapping = s -> begin
                res = findfirst(x -> x == s, state_space(DiamondArenaEnv))
                with_logger(logger) do
                    @logmsg Logging.Debug "Space mapping" s res
                end
                res
            end,
            state_space_mapping = _ -> Base.OneTo(length(state_space(DiamondArenaEnv))),
        );
        action_mapping = a -> begin
            res = action_space(DiamondArenaEnv)[a]
            with_logger(logger) do
                @logmsg Logging.Debug "Action mapping" a res
            end
            res
        end,
        action_space_mapping = _ -> Base.OneTo(length(action_space(DiamondArenaEnv))),
    )

    # Useful?
    # using MLBase
    # lm = labelmap(["a", "a", "b", "b", "c"])
    # labelencode(lm, ["a", "c", "b"])

    # RLBase.test_runnable!(DiamondArenaWrapped)

    NS, NA = length(state_space(DiamondArenaEnv)), length(action_space(DiamondArenaEnv))
    # policy = RandomPolicy()
    # policy = VBasedPolicy(
    #     learner = MonteCarloLearner(;
    #         approximator = TabularVApproximator(; n_state = NS, opt = InvDecay(1.0)),
    #         γ = 1.0,
    #         kind = FIRST_VISIT,
    #         sampling = NO_SAMPLING,
    #     ),
    # )
    # policy = QBasedPolicy(
    #     learner = MonteCarloLearner(;
    #         approximator = TabularQApproximator(;
    #             n_state = NS,
    #             n_action = NA,
    #             init = 0.0,
    #             opt = InvDecay(1.0),
    #         ),
    #         γ = 1.0,
    #         kind = FIRST_VISIT,
    #         sampling = NO_SAMPLING,
    #     ),
    #     explorer = EpsilonGreedyExplorer(0.1),
    # )
    # agent = Agent(
    #     policy = VBasedPolicy(
    #         learner = MonteCarloLearner(;
    #             approximator = TabularVApproximator(; n_state = NS, opt = InvDecay(1.0)),
    #             γ = 1.0,
    #             kind = FIRST_VISIT,
    #             sampling = NO_SAMPLING,
    #         ),
    #     ),
    #     trajectory = VectorSARTTrajectory(),
    # )

    # learner = MonteCarloLearner(;
    #     approximator = TabularQApproximator(;
    #         n_state = NS,
    #         n_action = NA,
    #         opt = InvDecay(1.0),
    #         # opt = Descent(1.0),
    #     ),
    #     γ = 0.2,
    #     kind = FIRST_VISIT,
    #     sampling = NO_SAMPLING,
    # )
    learner = TDLearner(
        approximator = TabularQApproximator(
            n_state = NS,
            n_action = NA,
            init = 0.0,
            opt = InvDecay(1.0),
        ),
        γ = 1.0,
        method = :ExpectedSARSA,
        # method = :SARSA,
        # method = :SARS,
        n = 0,
    )
    explorer = EpsilonGreedyExplorer(
        kind = :linear,
        ϵ_init = 0.9,
        ϵ_stable = 0.1,
        warmup_steps = 100,
        decay_steps = 100,
    )
    policy = QBasedPolicy(learner = learner, explorer = explorer)
    agent = Agent(policy = policy, trajectory = VectorSARTTrajectory())

    hook = TotalRewardPerEpisode(is_display_on_exit = true)
    # hook = RewardsPerEpisode()
    # stop_condition = StopAfterStep(100; is_show_progress = true)
    # n_episodes = 200
    stop_condition = StopAfterEpisode(n_episodes, is_show_progress = true)
    # n_runs = 3
    # run(policy, DiamondArenaWrapped, stop_condition, hook)

    # rewards = zeros(n_runs, n_episodes)
    # for rew = 1:n_runs
    run(agent, DiamondArenaWrapped, stop_condition, hook)
    # rewards[rew, :] = hook.rewards
    push!(rewards, hook.rewards)
    # end

    display(agent.policy.learner.approximator.table)
    # policy_tex = latexify(
    #     agent.policy.learner.approximator.table,
    #     env = :tabular,
    #     fmt = FancyNumberFormatter(3),
    #     booktabs = true,
    # )
    # open("policy.tex", "w") do file
    #     write(file, policy_tex)
    # end

    # title = "Total reward per episode"
    # plot!(
    #     p,
    #     # hook.rewards,
    #     rewards[i],
    #     # xlabel = "Episode",
    #     xlabel = "Episode",
    #     ylabel = "Cummulated rewards",
    #     # label = "Idle reward: $DO_NOTHING_NEG_REWARD",
    #     label = "Idle reward: $(trunc(Int, DO_NOTHING_NEG_REWARD))",
    #     title = title,
    #     legend=:bottomright,
    #     # legend = :outertopright,
    #     # markershape = :auto,
    #     yscale=:log10,
    #     background_color=:transparent,
    #     foreground_color = :black,
    #     size = (800, 600),
    #     dpi = 300,
    # )

end

# x = 1:n_episodes
# y = hook.rewards
# df = (; x, y)
# xy = data(df) * mapping(:x, :y)
# layer = visual(Lines)

df = DataFrame(
    episode = Int[],
    idle_reward = CategoricalValue{Float64,UInt32}[],
    reward = Float64[],
)
for (idx, item) in enumerate(rewards)
    rew = rewards[idx]
    ep = collect(1:length(rew))
    idle_reward = fill(categorical(do_nothing_reward)[idx], length(rew))
    for (jdx, _) in enumerate(rew)
        push!(df, (ep[jdx], idle_reward[jdx], rew[jdx]))
    end
end

function plotting()
    plt =
        data(df) *
        mapping(
            :episode => "Episode",
            :reward => "Cummulated rewards",
            # :idle_reward => "Reward when idle",
            color = :idle_reward => "Punishment when idle",
            # layout= :idle_reward,
        ) *
        visual(Lines)
    fg = draw(
        plt;
        # axis = (; yscale = Makie.Symlog10(10.0), title = title),
        axis = (; limits = (nothing, nothing, -700, 20), title = title),
        figure = (; backgroundcolor = :transparent),
        legend = (
            position = :bottom,
            titleposition = :left,
            framevisible = true,
            padding = 5,
        ),
    )
end

fn = plotsdir(title)
save("$fn.png", with_theme(plotting, theme_ggplot2()), px_per_unit = 3) # save high-resolution png
cmd = `xdg-open "$fn.png"`
Base.run(cmd)
# display(p)


# flush(io)
close(io)
