using DrWatson
@quickactivate "RL"

using Debugger
break_on(:error, :throw)

using Revise
using CSV
using DataFrames
using AlgebraOfGraphics, CairoMakie
using StatsBase
using CategoricalArrays


data_folder = datadir("exp_pro", "Wilson")
filenames = [
    joinpath(data_folder, "F1497_FM_Trace.csv"),
    joinpath(data_folder, "F1500_FM_Trace.csv"),
]
F1497 = DataFrame(CSV.File(filenames[1], header = [1, 2], normalizenames = true))
accepted = names(F1497)[[occursin("accepted", n) for n in names(F1497)]]
accepted_striped = [replace(acc, "_accepted" => "") for acc in accepted]

df = DataFrame(
    time = Float64[],
    Fluorescence = Float64[],
    C = CategoricalValue{String,UInt32}[],
)
cat = categorical(sample(accepted_striped, 10, replace = false))
for (idx, cat_val) in enumerate(cat)
    fluo = F1497[!, accepted[idx]]
    time = F1497[!, :_Time_s_Cell_Status]
    c = fill(cat_val, length(time))
    for (jdx, _) in enumerate(time)
        push!(df, (time[jdx], fluo[jdx], c[jdx]))
    end
end

function plotting()
    plt =
        data(df) *
        mapping(
            :time => "Time [s]",
            :Fluorescence,
            row = :C,
            color = :C,
            # :idle_reward => "Reward when idle",
            # color = :param => "Positive rewards",
            # layout= :idle_reward,
        ) *
        visual(Lines)
    fg = draw(
        plt;
        # axis = (; yscale = Makie.Symlog10(10.0), title = title),
        axis = (;
            backgroundcolor = :transparent,
            xgridcolor = :transparent,
            ygridcolor = :transparent,
            yminorgridvisible = false,
            grid = false,
            yticksvisible = false,
            yticklabelsvisible = false,
        ),
        # axis = (; limits = (nothing, nothing, -700, 20), title = title),
        figure = (; backgroundcolor = :transparent),
        # legend = (
        #     position = :bottom,
        #     titleposition = :left,
        #     framevisible = true,
        #     padding = 5,
        # ),
    )
    for leg in fg.figure.content
        if leg isa Legend
            delete!(leg)
        end
    end
    fg
end

title = "traces"
fn = plotsdir(title)
save("$fn.png", with_theme(plotting, theme_ggplot2()), px_per_unit = 3) # save high-resolution png
cmd = `xdg-open "$fn.png"`
Base.run(cmd)
