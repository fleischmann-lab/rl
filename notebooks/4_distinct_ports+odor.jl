# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .jl
#       format_name: hydrogen
#       format_version: '1.3'
#       jupytext_version: 1.14.4
#   kernelspec:
#     display_name: Julia 1.8.3
#     language: julia
#     name: julia-1.8
# ---

# %% [markdown]
# ## Init

# %%
using DrWatson
@quickactivate "RL"

# %%
using Debugger
break_on(:error, :throw)

using Revise
using ReinforcementLearning
using Flux
using Statistics
using Random
using DataFrames
using CSV
using LoggingExtras

# %%
includet(srcdir("utils.jl"))
includet(srcdir("4_distinct_ports+odor_env.jl"))
includet(srcdir("CustomHooks.jl"))
includet(srcdir("plotting.jl"))

# %%
io, logger = logging(Logging.Info)
# io, logger = logging(Logging.Debug)

# %% [markdown]
# ## Parameters

# %%
exp = (
    title = "4 ports + odor",
    env = "four_distinct_ports_odor_env",
    learner = "TabularQApproximator",
    explorer = "EpsilonGreedyExplorer",
)
p = (
    n_runs = 20,
    n_episodes = 400,
    #     rng = Random.seed!(42),
    rng = Random.GLOBAL_RNG,
    γ = 0.9,
    ϵ_init = 0.9,
    ϵ_stable = 0.1,
    warmup_steps = 100,
    decay_steps = 100,
    max_steps = 1000,
)

# %% [markdown]
# ## Simulation

# %%
env, _, _ = four_distinct_ports_odor_env()
NS, NA = length(state_space(env)), length(action_space(env))

rewards = zeros(p.n_episodes, p.n_runs)
steps = zeros(p.n_episodes, p.n_runs)
policy_tables = zeros(NA, NS, p.n_runs)
hook = nothing
for irun = 1:p.n_runs
    print("Run #$irun")
    learner = MonteCarloLearner(;
        approximator = TabularQApproximator(
            n_state = NS,
            n_action = NA,
            init = 0.0,
            opt = InvDecay(1.0),
        ),
        γ = p.γ,
    )
    explorer = EpsilonGreedyExplorer(
        kind = :linear,
        ϵ_init = p.ϵ_init,
        ϵ_stable = p.ϵ_stable,
        warmup_steps = p.warmup_steps,
        decay_steps = p.decay_steps,
        rng = p.rng,
    )
    policy = QBasedPolicy(learner = learner, explorer = explorer)
    agent = Agent(policy = policy, trajectory = VectorSARTTrajectory())
    hook =
        ComposedHook(TotalRewardPerEpisode(), StepsPerEpisode(), EpisodeBehaviorOdorObs())
    #     hook = RewardsPerEpisode()
    #     hook = ComposedHook(TotalRewardPerEpisode(), RewardsPerEpisode())
    stop_condition = StopAfterEpisode(p.n_episodes, is_show_progress = true)
    #     stop_condition = ComposedStopCondition(
    #         StopAfterEpisode(p.n_episodes, is_show_progress = true),
    #         StopAfterStep(p.max_steps; is_show_progress = true);
    #     )

    run(agent, env, stop_condition, hook)
    #     rewards[1:length(hook.hooks[1].rewards), irun] = hook.hooks[1].rewards
    rewards[:, irun] = hook.hooks[1].rewards
    # This is a bug, see:
    # https://github.com/JuliaReinforcementLearning/ReinforcementLearning.jl/issues/759
    #     steps[1:min(p.n_episodes, length(hook.hooks[2].steps)), irun] = hook.hooks[2].steps[1:min(p.n_episodes, length(hook.hooks[2].steps))]
    steps[:, irun] = hook.hooks[2].steps[1:p.n_episodes]
    policy_tables[:, :, irun] = agent.policy.learner.approximator.table
end
close(io)

# %%
rewards

# %%
hist = DataFrame(episode = Int[], rewards = Float64[], run = Int[])
for irun = 1:p.n_runs
    append!(
        hist,
        Dict(
            :episode => 1:p.n_episodes,
            :rewards => rewards[:, irun],
            :run => ones(p.n_episodes) .* irun,
        ),
    )
end
hist

# %%
# colors = Makie.wong_colors()
# fig = Makie.Figure(resolution = (800, 400))
# ax1 = Makie.Axis(fig[1, 1])
# ax2 = Makie.Axis(fig[1, 2])
# for line in 1:p.n_runs
#     scatterlines!(ax1, rewards[:, line])
#     hist!(ax2, hist.episode, hist.rewards,
#         dodge = hist.run,
#         color = colors[hist.run],
#         direction=:x)
# #    barplot!(ax2, hist.episode, hist.rewards,
# #        dodge = hist.run,
# #        color = colors[hist.run],
# #        direction=:x
# #    )
# end
# colsize!(fig.layout, 1, Relative(0.8))
# fig

# %% [markdown]
# ## Postprocessing

# %%
# Average policy values
policy_table = reshape(mean(policy_tables, dims = 3), (NA, NS))

# %%
df = DataFrame(
    episode = 1:p.n_episodes,
    reward = vec(mean(rewards, dims = 2)),
    step = vec(mean(steps, dims = 2)),
)
df.cum_reward = cumsum(df.reward)
df

# %%
# What happened during last episode
last_ep_behavior = hook[3]
ep = DataFrame(
    steps = last_ep_behavior.steps,
    N = last_ep_behavior.visitN,
    S = last_ep_behavior.visitS,
    E = last_ep_behavior.visitE,
    W = last_ep_behavior.visitW,
    Re = last_ep_behavior.rewards,
    start = last_ep_behavior.startEpisode,
    stop = last_ep_behavior.endEpisode,
)
csv_name = savename(
    Dict("title" => exp.title, "n_episodes" => p.n_episodes, "n_runs" => p.n_runs),
    "csv",
)
csvpath = datadir("sims", csv_name)
CSV.write(csvpath, ep)
print("Behavioral data saved at:\n$csvpath")
ep

# %%
ep = EpisodeBehaviorOdorObsPostProcess(csvpath)

# %%
# How many rewards did it get ?
ep[ep.RePort.>0, :]

# %% [markdown]
# ## Viz

# %%
create_plot(
    plot_avg_cum_rewards;
    exp = exp,
    params = p,
    backgroundcolor = :white,
    open_plot = false,
    df = df,
    rewards = rewards,
)

# %%
create_plot(
    plot_avg_steps;
    exp = exp,
    params = p,
    backgroundcolor = :white,
    open_plot = false,
    df = df,
    steps = steps,
)

# %%
policy = map(x -> x == 0.0 ? NaN : x, policy_table)

# %%
create_plot(
    plot_policy;
    exp = exp,
    params = p,
    backgroundcolor = :white,
    open_plot = false,
    theme = theme_light,
    policy_table = policy,
)

# %%
plot_visited_ports(
    df = ep,
    exp = exp,
    params = p,
    backgroundtransparent = false,
    saveplot = false,
)

# %%
ep_subset = ep[(ep.steps.>=0).&(ep.steps.<=1500), :]
plot_visited_ports(
    df = ep_subset,
    exp = exp,
    params = p,
    backgroundtransparent = false,
    saveplot = true,
)
