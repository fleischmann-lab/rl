# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .jl
#       format_name: hydrogen
#       format_version: '1.3'
#       jupytext_version: 1.14.4
#   kernelspec:
#     display_name: Julia 1.8.2
#     language: julia
#     name: julia-1.8
# ---

# %%
using DrWatson
@quickactivate "RL"
# using Conda
# Conda.add("seaborn")
# using PyPlot
# using PyCall
using CSV
using DataFrames
# @pyimport seaborn as sns
# sns.set(font_scale=2)
using Bokeh, BokehBlink

# %%
n_episodes = 500
title = "4 ports + odor"
csv_name = savename(Dict("title" => title, "n_episodes" => n_episodes), "csv")
csvpath = datadir("sims", csv_name)

# %%
function EpisodeBehaviorOdorObsPostProcess(csvpath)
    df = CSV.File(csvpath) |> DataFrame

    # Give a nuber to each port for plotting
    df.S = df.S * 2
    df.E = df.E * 3
    df.W = df.W * 4

    # Get the ports as a chronological sequence
    df.sequence = df.N .| df.S .| df.E .| df.W

    # Get the port of the rewarded/correct episodes
    diffRe = prepend!(diff(circshift(df.Re, -1)), 0)
    df.RePort = circshift(df.stop, -2)
    df.RePort[df.RePort.>0] = df.E[df.RePort.>0] .| df.W[df.RePort.>0]
    df.RePort[diffRe.≠10] .= 0

    # Get the port starting the episode
    df.StartPort = zeros(length(df.start))
    start_idx = findall(df.start .> 0)
    for idx ∈ start_idx
        nextN = findnext(df.N .> 0, idx)
        nextS = findnext(df.S .> 0, idx)

        # Workaround for bug or strange behavior as
        # the environment seems to be restarted a few times at the end
        nextidx = filter!(x -> x != nothing, [nextN, nextS])
        if length(nextidx) == 2
            idx_port = min(nextN, nextS)
        elseif length(nextidx) == 1
            idx_port = nextidx[1]
        end
        if @isdefined idx_port
            df.StartPort[idx_port] = df.sequence[idx_port]
        end
    end
    return df
end

# %%
df = EpisodeBehaviorOdorObsPostProcess(csvpath)

# %%
# function plot_visited_ports(df)
#     PyPlot.figure()
#     fig, ax = plt.subplots(figsize = (25, 10))
#     ax.set_yticks([1, 2, 3, 4])
#     ax.set(xlabel = "Steps", ylabel = "Ports", title = title)
#     ax.set_yticklabels(["North", "South", "East", "West"])
#     cmap = sns.color_palette("tab10")
#     sns.scatterplot(
#         x = df.steps[df.StartPort.>0],
#         y = df.StartPort[df.StartPort.>0],
#         color = "gold",
#         s = 400,
#         marker = "o",
#         legend = "auto",
#         label = "Odor cue",
#     )
#     sns.scatterplot(
#         x = df.steps[df.RePort.>0],
#         y = df.RePort[df.RePort.>0],
#         color = "cyan",
#         s = 400,
#         marker = "o",
#         legend = "auto",
#         label = "Reward"
#     )
#     sns.scatterplot(
#         x = df.steps[df.N.>0],
#         y = df.N[df.N.>0],
#         color = cmap[1],
#         marker = "o",
#         legend = "auto",
#     )
#     sns.scatterplot(
#         x = df.steps[df.S.>0],
#         y = df.S[df.S.>0],
#         color = cmap[2],
#         marker = "o",
#         legend = "auto",
#     )
#     sns.scatterplot(
#         x = df.steps[df.E.>0],
#         y = df.E[df.E.>0],
#         color = cmap[3],
#         marker = "o",
#         legend = "auto",
#     )
#     sns.scatterplot(
#         x = df.steps[df.W.>0],
#         y = df.W[df.W.>0],
#         color = cmap[4],
#         marker = "o",
#         legend = "auto",
#     )
#     sns.lineplot(
#         x = df.steps[df.sequence.>0],
#         y = df.sequence[df.sequence.>0],
#         legend = false,
#         alpha = 0.3,
#     )
#     return fig
# end

# %%
plot_visited_ports(df)
plot_name = savename(
    Dict("plot" => "visited_ports", "title" => title, "n_episodes" => n_episodes),
    "png",
)
plotpath = plotsdir(plot_name)
PyPlot.savefig(plotpath, facecolor = "none", dpi = 300)

# %%
# How many rewards did it get ?
df[df.RePort.>0, :]

# %%
df_subset = df[(df.steps.>=0).&(df.steps.<=1500), :]

# %%
plot_visited_ports(df_subset);

# %%
function plot_visited_ports(df)
    p = figure(
        width = 1000,
        height = 600,
        title = "Visited ports",
        x_axis_label = "Steps",
        y_axis_label = "Ports",
        background_fill_color = "#fafafa",
    )
    p.y_axis.ticker = [1, 2, 3, 4]
    p.y_axis.major_label_overrides =
        Dict("1" => "North", "2" => "South", "3" => "East", "4" => "West")
    colormap = ["#1f77b4", "#ff7f0e", "#2ca02c", "#d62728"]

    plot!(
        p,
        Line;
        x = df.steps[df.sequence.>0],
        y = df.sequence[df.sequence.>0],
        line_alpha = 0.3,
    )
    plot!(
        p,
        Scatter,
        x = x = df.steps[df.StartPort.>0],
        y = df.StartPort[df.StartPort.>0],
        legend_label = "Odor cue",
        fill_alpha = 0.4,
        size = 12,
        color = "gold",
    )
    plot!(
        p,
        Scatter,
        x = df.steps[df.RePort.>0],
        y = df.RePort[df.RePort.>0],
        legend_label = "Reward",
        fill_alpha = 0.4,
        size = 12,
        color = "cyan",
    )
    plot!(
        p,
        Scatter,
        x = df.steps[df.N.>0],
        y = df.N[df.N.>0],
        #         fill_alpha=0.4,
        #         size=12,
        color = colormap[1],
    )
    plot!(
        p,
        Scatter,
        x = df.steps[df.S.>0],
        y = df.S[df.S.>0],
        #         fill_alpha=0.4,
        #         size=12,
        color = colormap[2],
    )
    plot!(
        p,
        Scatter,
        x = df.steps[df.E.>0],
        y = df.E[df.E.>0],
        #         fill_alpha=0.4,
        #         size=12,
        color = colormap[3],
    )
    plot!(
        p,
        Scatter,
        x = df.steps[df.W.>0],
        y = df.W[df.W.>0],
        #         fill_alpha=0.4,
        #         size=12,
        color = colormap[4],
    )
    return p
end

# %%
p = plot_visited_ports(df)
plot_name = savename(
    Dict("plot" => "visited_ports", "title" => title, "n_episodes" => n_episodes),
    "png",
)
plotpath = plotsdir(plot_name)
BokehBlink.save(plotpath, p)
print(plotpath)
p

# %%
plot_visited_ports(df_subset)

# %%
