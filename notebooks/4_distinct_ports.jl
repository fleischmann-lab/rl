# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .jl
#       format_name: hydrogen
#       format_version: '1.3'
#       jupytext_version: 1.14.1
#   kernelspec:
#     display_name: Julia 1.8.2
#     language: julia
#     name: julia-1.8
# ---

# %%
using DrWatson
@quickactivate "RL"

# %%
using Debugger
break_on(:error, :throw)

using Revise
using ReinforcementLearning
using Flux
using Statistics
using Random
using DataFrames
using CSV
using LoggingExtras

# %%
includet(srcdir("utils.jl"))
includet(srcdir("4_distinct_ports_env.jl"))
includet(srcdir("CustomHooks.jl"))
includet(srcdir("plotting.jl"))
io, logger = logging(Logging.Info)

# %%
n_runs = 10
n_episodes = 200
title = "4 ports model"
# Random.seed!(42)


env, _, _ = four_distinct_ports_env()
NS, NA = length(state_space(env)), length(action_space(env))

learner = MonteCarloLearner(;
    approximator = TabularQApproximator(
        n_state = NS,
        n_action = NA,
        init = 0.0,
        opt = InvDecay(1.0),
    ),
    γ = 1.0,
)
explorer = EpsilonGreedyExplorer(
    kind = :linear,
    ϵ_init = 0.9,
    ϵ_stable = 0.1,
    warmup_steps = 100,
    decay_steps = 100,
)
policy = QBasedPolicy(learner = learner, explorer = explorer)
agent = Agent(policy = policy, trajectory = VectorSARTTrajectory())
# hook = TotalRewardPerEpisode(is_display_on_exit = true)
hook = TotalRewardAndStepsPerEpisode()
# hook = EpisodeBehavior()
stop_condition = StopAfterEpisode(n_episodes, is_show_progress = true)

rewards = zeros(n_episodes, n_runs)
steps = zeros(n_episodes, n_runs)
policy_tables = zeros(NA, NS, n_runs)
for irun = 1:n_runs
    run(agent, env, stop_condition, hook)
    rewards[:, irun] = hook.rewards
    steps[:, irun] = hook.steps
    policy_tables[:, :, irun] = agent.policy.learner.approximator.table
end

# %%
df = DataFrame(
    episode = 1:n_episodes,
    reward = vec(mean(rewards, dims = 2)),
    step = vec(mean(steps, dims = 2)),
)

# %%
policy_table = reshape(mean(policy_tables, dims = 3), (NA, NS))

# %%
create_plot(
    plot_avg_cum_rewards;
    title = title,
    backgroundcolor = :white,
    open_plot = false,
    n_runs = n_runs,
    n_episodes = n_episodes,
    df = df,
    rewards = rewards,
)

# %%
create_plot(
    plot_avg_steps;
    title = title,
    backgroundcolor = :white,
    open_plot = false,
    n_runs = n_runs,
    n_episodes = n_episodes,
    df = df,
    steps = steps,
)

# %%
create_plot(
    plot_policy;
    title = title,
    backgroundcolor = :white,
    open_plot = false,
    theme = theme_light,
    policy_table = agent.policy.learner.approximator.table,
)
