# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown] id="54AIvDov_7aa" tags=[]
# ## Dependencies

# %%
import os

os.chdir("../")

# %% colab={"base_uri": "https://localhost:8080/"} id="gxxpHDIs_lvg" outputId="412c65c1-7c24-4b9a-f5cf-8f98dd648fe4" tags=[]
import numpy as np
import gymnasium as gym
import random
from tqdm import tqdm
import matplotlib.pyplot as plt
from gymnasium.utils.save_video import save_video

from src import gridworld

# %load_ext lab_black

# %% [markdown]
# ## Parameters

# %% id="NP28hCqLiVSy"
# Hyperparameters
total_episodes = 100  # Total episodes
learning_rate = 0.8  # Learning rate
max_steps = 99  # Max steps per episode
gamma = 0.95  # Discounting rate

# Exploration parameters
epsilon = 1.0  # Exploration rate
max_epsilon = 1.0  # Exploration probability at start
min_epsilon = 0.01  # Minimum exploration probability
decay_rate = 0.001  # Exponential decay rate for exploration prob

# %% [markdown] id="0fz-X3HTQueX"
# ## Gridworld environment

# %% id="mh9jBR_cQ5_a"
# env = gym.make("DiamondEnv-v0", render_mode="rgb_array_list")
env = gym.make("DiamondEnv-v0", render_mode="human")

# %% [markdown] id="JEtXMldxQ7uw"
# ### Creating the Q-table
# Now, let's create our Q-table initialized at zero with the states number as rows and the actions number as columns.

# %% colab={"base_uri": "https://localhost:8080/"} id="Uc0xDVd_Q-C8" outputId="927b86ea-5a41-401a-d81a-99dd183cea55"
action_size = env.action_space.n
state_size = env.observation_space.n
print(f"Action size: {action_size}")
print(f"State size: {state_size}")
qtable = np.zeros((state_size, action_size))

# %% colab={"base_uri": "https://localhost:8080/"} id="4-jH3XteiIuH" outputId="56e34cbb-60ec-410d-81e7-0162a0ce026f"
# Define a seed so that we get reproducible results
seed = 42

# %% colab={"base_uri": "https://localhost:8080/"} id="uyEdi0gGiV1z" outputId="b644224d-13ff-4806-d751-0088efc24f85"
rewards = []
for episode in tqdm(range(total_episodes)):
    state = env.reset(seed=seed)[0]  # Reset the environment
    step = 0
    done = False
    total_rewards = 0

    for step in range(max_steps):
        #         print(f"start step...")
        # 3. Choose an action a in the current world state (s)
        ## First we randomize a number
        exp_exp_tradeoff = random.uniform(0, 1)

        #         print(f"exp_exp_tradeoff: {exp_exp_tradeoff}")

        ## If this number > greater than epsilon --> exploitation
        # (taking the biggest Q value for this state)
        if exp_exp_tradeoff > epsilon:
            #             print(f"qtable[state,:] {qtable[state,:]}")
            action = np.argmax(qtable[state, :])

        # Else doing a random choice --> exploration
        else:
            action = env.action_space.sample()

        #         print(f"action is {action}")

        # Take the action (a) and observe the outcome state(s') and reward (r)
        new_state, reward, terminated, truncated, info = env.step(action)

        done = terminated or truncated
        # print(f"new_state: {new_state}, reward: {reward}, done: {done}, info: {info}")

        # Update Q(s,a):= Q(s,a) + lr [R(s,a) + gamma * max Q(s',a') - Q(s,a)]
        # qtable[new_state,:] : all the actions we can take from new state
        qtable[state, action] = qtable[state, action] + learning_rate * (
            reward + gamma * np.max(qtable[new_state, :]) - qtable[state, action]
        )

        #         print(f'qtable: {qtable}')

        total_rewards = total_rewards + reward

        #         print(f'total_rewards {total_rewards}')

        # Our new state is state
        state = new_state

        #         print(f'new state: {state}')

        # If done (if we're dead) : finish episode
        if done == True:
            break

    episode += 1
    # Reduce epsilon (because we need less and less exploration)
    epsilon = min_epsilon + (max_epsilon - min_epsilon) * np.exp(-decay_rate * episode)
    rewards.append(total_rewards)

print("Score over time: " + str(sum(rewards) / total_episodes))
print(qtable)
print(f"epsilon = {epsilon}")

# %% colab={"base_uri": "https://localhost:8080/", "height": 317} id="pk8lHOjiL7fa" outputId="0474335e-bf29-4be1-d877-e6adc5a13afc"
# Print the action in every place
# LEFT = 0 DOWN = 1 RIGHT = 2 UP = 3

env.reset(seed=seed)
# img = plt.imshow(env.render()[0])
# plt.axis('off')
# display.display(plt.gcf())
print(np.argmax(qtable, axis=1).reshape(4, 4))

# %% colab={"base_uri": "https://localhost:8080/", "height": 422} id="RW6HkwClL8y6" outputId="ca3ed3b5-ea77-4ecb-c70a-860a308c79f0"
# All the episoded is the same
# taking the maximum of Qtable value every time.
env.reset(seed=seed)
episode_index = 0
for episode in range(5):
    state = env.reset(seed=seed)[0]
    step = 0
    done = False
    print("EPISODE ", episode)

    step_starting_index = 0
    for step in range(max_steps):
        # display.clear_output(wait=True)
        # img.set_data(env.render()[0])
        # plt.axis('off')
        # Take the action (index) that have the maximum expected future reward given that state
        action = np.argmax(qtable[state, :])

        new_state, reward, terminated, truncated, info = env.step(action)

        done = terminated or truncated
        if done:
            gym.utils.save_video.save_video(
                env.render(),
                "data/sims/videos",
                fps=env.metadata["render_fps"],
                step_starting_index=step_starting_index,
                episode_index=episode_index,
            )
            break
        step_starting_index = episode_index + step + 1
        state = new_state
    episode_index += 1

# %% id="Xj1z5ob10ltw"
env.close()

# %%
from IPython.display import Video

Video(
    "../data/sims/videos/rl-video-episode-0.mp4",
    embed=True,
)
