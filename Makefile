help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

format: ## Autoformat everything
	julia -e 'using Pkg; Pkg.add("JuliaFormatter"); using JuliaFormatter; if format(".") == true println("Formatting OK ✔"); exit(0) else println("Formatting KO ❌"); exit(1) end'

.ONESHELL:
convert-nb: ## Convert notebook JSON to plain text
	. venv/bin/activate
	jupytext --from ipynb --to jl:hydrogen notebooks/4_distinct_ports+odor.ipynb
	jupytext --from ipynb --to jl:hydrogen notebooks/visited_ports.ipynb

# check-nb:
# 	make convert-nb > make.log 2>&1
# 	# if [ "$(grep -i 'writing' make.log)" -eq 0 ]; then exit 1; fi
# 	# rm make.log
# 	test := $(shell grep -i 'writing' make.log)
# 	ifeq (test,1)
# 	echo "haha"
# 	endif
