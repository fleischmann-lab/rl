### A Pluto.jl notebook ###
# v0.17.7

using Markdown
using InteractiveUtils

# ╔═╡ eec15b4c-78aa-11ec-1126-ef2e9749c8b3
begin
    # Activate the environment
    using Pkg
    Pkg.activate(".")

    # Load the packages
    using VegaLite
    using Statistics
    using DataFrames
    using ReinforcementLearning

    # Load our custom functions
    include("DiamondArenaEnv.jl")
end

# ╔═╡ 15ba2908-2b53-436f-a337-a4a40e4986b9
function simulate()
    hook = TotalRewardPerEpisode(is_display_on_exit = false)
    # hook = RewardsPerEpisode()
    # stop_condition = StopAfterStep(50; is_show_progress = true)
    stop_condition = StopAfterEpisode(1_000, is_show_progress = true)
    # run(policy, DiamondArenaWrapped, stop_condition, hook)

    rewards = []
    for _ = 1:3
        run(agent, DiamondArenaWrapped, stop_condition, hook)
        push!(rewards, hook.rewards)
        hook.rewards[1:10]
    end
    return rewards
end

# ╔═╡ 0114ecc4-20ce-441f-b725-e83f8dd67d9b
rewards = simulate()

# ╔═╡ ddd65ae0-55c1-4c82-8a7b-643fd7c78c06
DataFrame(agent.policy.learner.approximator.table, :auto)

# ╔═╡ c0a19b59-bf44-4f09-8c9a-c27b828eb643
begin
    n_runs = length(rewards)

    data = DataFrame(episode = Int[], run = Int[], reward = Float64[])
    for (idx, item) in enumerate(rewards)
        rew = rewards[idx]
        ep = collect(1:length(rew))
        run = fill(idx, length(rew))
        for (jdx, _) in enumerate(rew)
            push!(data, (ep[jdx], run[jdx], rew[jdx]))
        end
    end
    data
end

# ╔═╡ 85bcd859-785d-4667-ad40-ec9662dc8552
const markColor = "#000"

# ╔═╡ 51bf4396-3a6c-4ffb-82ef-4d08d2ea624b
data |>
@vlplot(
    width = 600,
    height = 400,
    x = :episode,
    config = {
        group = {fill = "#e5e5e5"},
        arc = {fill = markColor},
        area = {fill = markColor},
        line = {stroke = markColor},
        path = {stroke = markColor},
        rect = {fill = markColor},
        shape = {stroke = markColor},
        symbol = {fill = markColor, size = 40},
        axis = {
            domain = false,
            grid = true,
            gridColor = "#FFFFFF",
            gridOpacity = 1,
            labelColor = "#7F7F7F",
            labelPadding = 4,
            tickColor = "#7F7F7F",
            tickSize = 5.67,
            titleFontSize = 16,
            titleFontWeight = "normal",
        },
        legend = {labelBaseline = "middle", labelFontSize = 11, symbolSize = 40},
        range = {
            category = [
                "#000000",
                "#7F7F7F",
                "#1A1A1A",
                "#999999",
                "#333333",
                "#B0B0B0",
                "#4D4D4D",
                "#C9C9C9",
                "#666666",
                "#DCDCDC",
            ],
        },
    }
) +
@vlplot(mark = {:errorband, extent = :ci}, y = {:reward, title = ""}) +
@vlplot(
    :line,
    y = {
        :reward,
        scale = {type = "symlog"},
        title = "Cumulated reward averaged over $n_runs runs",
        aggregate = "mean",
    },
)

# ╔═╡ Cell order:
# ╠═eec15b4c-78aa-11ec-1126-ef2e9749c8b3
# ╠═15ba2908-2b53-436f-a337-a4a40e4986b9
# ╠═0114ecc4-20ce-441f-b725-e83f8dd67d9b
# ╠═ddd65ae0-55c1-4c82-8a7b-643fd7c78c06
# ╠═c0a19b59-bf44-4f09-8c9a-c27b828eb643
# ╠═85bcd859-785d-4667-ad40-ec9662dc8552
# ╠═51bf4396-3a6c-4ffb-82ef-4d08d2ea624b
