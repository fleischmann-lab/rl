using HypothesisTests
using BSON: @save
using AlgebraOfGraphics, CairoMakie
using Bokeh, BokehBlink


function create_plot(
    plot_func::Function;
    exp::NamedTuple,
    params::NamedTuple,
    backgroundcolor::Symbol = :transparent,
    open_plot::Bool = false,
    theme::Function = theme_ggplot2,
    kwargs...,
)
    fn = plotsdir(
        savename(
            Dict(
                "plot" => "$(plot_func)",
                "title" => exp.title,
                "n_episodes" => params.n_episodes,
                "n_runs" => params.n_runs,
            ),
            "png",
        ),
    )
    fg = with_theme(
        () -> plot_func(;
            exp = exp,
            params = params,
            backgroundcolor = backgroundcolor,
            kwargs...,
        ),
        theme(),
    )
    save(fn, fg, px_per_unit = 3) # save high-resolution png
    print("Plot saved at:\n$fn")

    if open_plot
        cmd = `xdg-open "$fn"`
        Base.run(cmd)
    end
    return fg
end

"Plot cummulated rewards"
function plot_avg_cum_rewards(;
    backgroundcolor::Symbol = :transparent,
    exp::NamedTuple,
    params::NamedTuple,
    df::DataFrame,
    rewards::Matrix{Float64},
)
    lower, upper = zeros(params.n_episodes), zeros(params.n_episodes)
    for ep = 1:params.n_episodes
        lower[ep], upper[ep] = confint(OneSampleTTest(rewards[ep, :]))
    end
    @save datadir(
        "sims",
        savename(
            Dict(
                "title" => exp.title,
                "n_episodes" => params.n_episodes,
                "n_runs" => params.n_runs,
            ),
            "bson",
        ),
    ) df upper lower exp params

    plt =
        data(df) *
        mapping(
            :episode => "Episode",
            # :reward => "Log of cummulated rewards averaged over $(params.n_runs) runs",
            # :reward => "Log of rewards averaged over $(params.n_runs) runs",
            :reward => "Rewards averaged over $(params.n_runs) runs",
        ) *
        visual(Lines)
    fg = draw(
        plt;
        axis = (;
            # yscale = Makie.pseudolog10,
            title = exp.title
        ),
        figure = (; backgroundcolor = backgroundcolor),
    )
    linesfill!(df.episode, df.reward; lower = lower, upper = upper)  # plot stddev band
    return fg
end
"Plot number of steps per episode"
function plot_avg_steps(;
    backgroundcolor::Symbol = :transparent,
    exp::NamedTuple,
    params::NamedTuple,
    df::DataFrame,
    steps::Matrix{Float64},
)
    lower, upper = zeros(params.n_episodes), zeros(params.n_episodes)
    for ep = 1:params.n_episodes
        lower[ep], upper[ep] = confint(OneSampleTTest(steps[ep, :]))
    end
    @save datadir(
        "sims",
        savename(
            Dict(
                "title" => exp.title,
                "n_episodes" => params.n_episodes,
                "n_runs" => params.n_runs,
            ),
            "bson",
        ),
    ) df upper lower exp params

    plt =
        data(df) *
        mapping(
            :episode => "Episode",
            # :step => "Log of steps number averaged over $(params.n_runs) runs",
            :step => "Steps number averaged over $(params.n_runs) runs",
        ) *
        visual(Lines)
    fg = draw(
        plt;
        axis = (;
            # yscale = Makie.pseudolog10,
            title = exp.title
        ),
        figure = (; backgroundcolor = backgroundcolor),
    )
    linesfill!(df.episode, df.step; lower = lower, upper = upper)  # plot stddev band
    return fg
end

"Plot tabular policy"
function plot_policy(;
    exp::NamedTuple,
    params::NamedTuple,
    backgroundcolor::Symbol = :transparent,
    policy_table::Matrix{Float64},
)
    fig = Makie.Figure(resolution = (1300, 600), backgroundcolor = backgroundcolor)
    ax =
        fig[1, 1] = Makie.Axis(
            fig,
            xlabel = "𝗦𝘁𝗮𝘁𝗲𝘀", # States string in bold unicode
            ylabel = "𝗔𝗰𝘁𝗶𝗼𝗻𝘀", # Actions string in bold unicode
            xticklabelrotation = 45.0,
            yticks = (
                1:1:NA,
                [String(Symbol(item)) for item in reverse(action_space(DiamondArenaEnv))],
            ),
            xticks = (
                1:1:NS,
                [String(Symbol(item)) for item in state_space(DiamondArenaEnv)],
            ),
            bottomspinevisible = false,
            leftspinevisible = false,
        )
    # values = reverse(agent.policy.learner.approximator.table, dims=2)
    values = rotr90(policy_table)
    hm = heatmap!(ax, values, colormap = :plasma)
    Colorbar(fig[1, 2], hm)
    text!(
        ax,
        ifelse.(
            isnan.(vec(values')) .== true,
            "",
            string.(round.(vec(values'), digits = 2)),
        ),
        position = [Point2f(x, y) for x = 1:size(values)[1] for y = 1:size(values)[2]],
        align = (:center, :center),
        color = ifelse.(vec(values') .< mean(filter(!isnan, values)), :white, :black),
        textsize = 12,
    )
    return fig
end

"Interactive plot to look at visited ports"
function plot_visited_ports(;
    df::DataFrame,
    exp::NamedTuple,
    params::NamedTuple,
    backgroundtransparent::Bool = true,
    saveplot::Bool = true,
)
    ggplot_theme = Bokeh.load_theme(srcdir("ggplot.json"))
    Bokeh.settings!(theme = ggplot_theme)
    if backgroundtransparent == true
        Bokeh.theme!("Figure.border_fill_alpha" => 0.0)
    else
        Bokeh.theme!("Figure.border_fill_alpha" => 1.0)
    end
    p = Bokeh.figure(
        width = 1000,
        height = 600,
        title = "Visited ports",
        x_axis_label = "Steps",
        y_axis_label = "Ports",
        # background_fill_color = "#fafafa",
    )
    p.y_axis.ticker = [1, 2, 3, 4]
    p.y_axis.major_label_overrides =
        Dict("1" => "North", "2" => "South", "3" => "East", "4" => "West")
    colormap = ["#1f77b4", "#ff7f0e", "#2ca02c", "#d62728"]

    Bokeh.plot!(
        p,
        Line;
        x = df.steps[df.sequence.>0],
        y = df.sequence[df.sequence.>0],
        line_alpha = 0.3,
    )
    Bokeh.plot!(
        p,
        Bokeh.Scatter,
        x = x = df.steps[df.StartPort.>0],
        y = df.StartPort[df.StartPort.>0],
        legend_label = "Odor cue",
        fill_alpha = 0.5,
        size = 12,
        color = "gold",
    )
    Bokeh.plot!(
        p,
        Bokeh.Scatter,
        x = df.steps[df.RePort.>0],
        y = df.RePort[df.RePort.>0],
        legend_label = "Reward",
        fill_alpha = 0.4,
        size = 12,
        color = "cyan",
    )
    Bokeh.plot!(
        p,
        Bokeh.Scatter,
        x = df.steps[df.N.>0],
        y = df.N[df.N.>0],
        #         fill_alpha=0.4,
        #         size=12,
        color = colormap[1],
    )
    Bokeh.plot!(
        p,
        Bokeh.Scatter,
        x = df.steps[df.S.>0],
        y = df.S[df.S.>0],
        #         fill_alpha=0.4,
        #         size=12,
        color = colormap[2],
    )
    Bokeh.plot!(
        p,
        Bokeh.Scatter,
        x = df.steps[df.E.>0],
        y = df.E[df.E.>0],
        #         fill_alpha=0.4,
        #         size=12,
        color = colormap[3],
    )
    Bokeh.plot!(
        p,
        Bokeh.Scatter,
        x = df.steps[df.W.>0],
        y = df.W[df.W.>0],
        #         fill_alpha=0.4,
        #         size=12,
        color = colormap[4],
    )

    if saveplot == true
        plot_name = savename(
            Dict(
                "plot" => "visited_ports",
                "title" => exp.title,
                "n_episodes" => params.n_episodes,
                "n_runs" => params.n_runs,
            ),
            "png",
        )
        plotpath = plotsdir(plot_name)
        BokehBlink.save(plotpath, p)
        print("Plot saved at:\n$plotpath")
    end

    return p
end
