using ReinforcementLearning
# using PrettyPrinting


DO_NOTHING_NEG_REWARD = 0.0
NO_REWARD = 0.0
EndOfEpisode = "EndOfEpisode"
StartOfEpisode = "Arena"
SMALL_REWARD = 0.0
BIG_REWARD = 10.0

Base.@kwdef mutable struct DiamondArenaMdp <: AbstractEnv
    current_state::String = StartOfEpisode
    reward::Float64 = 0.0
end

MDP = (
    Arena = (
        DoNothing = (next_state = "Arena", reward = DO_NOTHING_NEG_REWARD),
        MoveCue = (next_state = "CuePort", reward = NO_REWARD),
        MoveOther = (next_state = "OtherPort", reward = NO_REWARD),
        MoveArena = (next_state = "Arena", reward = DO_NOTHING_NEG_REWARD),
    ),
    CuePort = (
        DoNothing = (next_state = "CuePort", reward = DO_NOTHING_NEG_REWARD),
        PokePlusSniff = (next_state = "Odor", reward = SMALL_REWARD),
        MoveArena = (next_state = "Arena", reward = NO_REWARD),
    ),
    OtherPort = (
        DoNothing = (next_state = "OtherPort", reward = DO_NOTHING_NEG_REWARD),
        MoveCue = (next_state = "CuePort", reward = NO_REWARD),
        MoveArena = (next_state = "Arena", reward = DO_NOTHING_NEG_REWARD),
    ),
    Odor = (
        DoNothing = (next_state = "Odor", reward = DO_NOTHING_NEG_REWARD),
        MoveLeftWest = (next_state = "RewardPort", reward = NO_REWARD),
        MoveOther = (next_state = "OtherPortPost", reward = NO_REWARD),
        MoveArena = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
    ),
    RewardPort = (
        DoNothing = (next_state = "RewardPort", reward = DO_NOTHING_NEG_REWARD),
        PokePlusLick = (next_state = EndOfEpisode, reward = BIG_REWARD),
        MoveArena = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
    ),
    ArenaPost = (
        DoNothing = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
        MoveLeftWest = (next_state = "RewardPort", reward = NO_REWARD),
        MoveOther = (next_state = "OtherPortPost", reward = NO_REWARD),
    ),
    OtherPortPost = (
        DoNothing = (next_state = "OtherPortPost", reward = DO_NOTHING_NEG_REWARD),
        Poke = (next_state = EndOfEpisode, reward = NO_REWARD),
        MoveArena = (next_state = "ArenaPost", reward = DO_NOTHING_NEG_REWARD),
    ),
    EndOfEpisode = (Restart = (next_state = StartOfEpisode, reward = NO_REWARD),),
)

# Minimal Interfaces to Implement
RLBase.action_space(env::DiamondArenaMdp) = [
    string(item) for item in
    unique(collect(Iterators.flatten([keys(MDP[state]) for state in keys(MDP)])))
]
RLBase.reward(env::DiamondArenaMdp) = env.reward
RLBase.state(env::DiamondArenaMdp) = env.current_state
RLBase.state_space(env::DiamondArenaMdp) = [string(item) for item in keys(MDP)]
RLBase.is_terminated(env::DiamondArenaMdp) = state(env) == EndOfEpisode
function RLBase.reset!(env::DiamondArenaMdp)
    env.current_state = StartOfEpisode
    env.reward = 0.0
    nothing
end
RLBase.ChanceStyle(env::DiamondArenaMdp) = RLBase.DETERMINISTIC
RLBase.ActionStyle(env::DiamondArenaMdp) = FULL_ACTION_SET
RLBase.legal_action_space(env::DiamondArenaMdp) =
    RLBase.action_space(env)[legal_action_space_mask(env)]
function RLBase.legal_action_space_mask(env::DiamondArenaMdp)
    mask = fill(false, size(RLBase.action_space(env)))
    allowed_actions = [string(item) for item in keys(MDP[Symbol(state(env))])]
    for action ∈ allowed_actions
        mask = mask .|| (RLBase.action_space(env) .== action)
    end
    mask
end

"Environment logic"
function (env::DiamondArenaMdp)(action::String)
    original_state = state(env)  # Needs to store the state otherwise it gets overwriten
    env.current_state = MDP[Symbol(original_state)][Symbol(action)][:next_state]
    env.reward += MDP[Symbol(original_state)][Symbol(action)][:reward]
    with_logger(logger) do
        @debug("Original state: $original_state")
        @debug("Action: $action")
        @debug("New state: $(env.current_state)")
        @debug("Reward: $(env.reward)")
    end
    nothing
end

DiamondArenaEnv = DiamondArenaMdp()
# RLBase.test_runnable!(DiamondArenaMdp())

DiamondArenaWrapped = ActionTransformedEnv(
    StateTransformedEnv(
        DiamondArenaEnv;
        state_mapping = s -> begin
            res = findfirst(x -> x == s, state_space(DiamondArenaEnv))
            with_logger(logger) do
                @logmsg Logging.Debug "Space mapping" s res
            end
            res
        end,
        state_space_mapping = _ -> Base.OneTo(length(state_space(DiamondArenaEnv))),
    );
    action_mapping = a -> begin
        res = action_space(DiamondArenaEnv)[a]
        with_logger(logger) do
            @logmsg Logging.Debug "Action mapping" a res
        end
        res
    end,
    action_space_mapping = _ -> Base.OneTo(length(action_space(DiamondArenaEnv))),
)


NS, NA = length(state_space(DiamondArenaEnv)), length(action_space(DiamondArenaEnv))


"Simplified diamond arena env"
function diamond_arena_simplified_env()
    return DiamondArenaWrapped, NS, NA
end
