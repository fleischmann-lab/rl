# using DrWatson
# @quickactivate "RL"
using ReinforcementLearning
using OrderedCollections
# using PrettyPrinting

# include(srcdir("utils.jl"))
# io, logger = logging(Logging.Debug)


const OdorCondition = Set([:pre, :post])
const OdorID = OrderedSet([:NoOdor, :OdorA, :OdorB])
# OdorID = OrderedSet([:NoOdor, :OdorA])
const CuePorts = Set([:NorthPort, :SouthPort])
const RewardPorts = Set([:East, :West])
const States =
    OrderedSet([:Arena, :NorthPort, :SouthPort, :EastPort, :WestPort, :EndOfEpisode])
const Actions = OrderedSet([
    :DoNothing,
    :MoveNorth,
    :MoveSouth,
    :PokePlusSniff,
    :MoveEast,
    :MoveWest,
    :MoveArena,
    :PokePlusLick,
    :Poke,
    :Restart,
])

function initialize_episode()
    cue_port = sample(collect(CuePorts))
    # cue_port = :NorthPort
    odor_ID = sample([o for o in collect(OdorID) if startswith(String(o), "Odor")])
    return (; cue_port, odor_ID)
end
function get_reward_port(cue_port::Symbol, odor_ID::Symbol)::Symbol
    if cue_port == :NorthPort && odor_ID == :OdorA
        reward_port = :WestPort
    elseif cue_port == :SouthPort && odor_ID == :OdorA
        reward_port = :WestPort
    elseif cue_port == :SouthPort && odor_ID == :OdorB
        reward_port = :EastPort
    elseif cue_port == :NorthPort && odor_ID == :OdorB
        reward_port = :EastPort
    else
        error("Combination of cue port $cue_port and odor ID $odor_ID not possible")
    end
    # reward_port = :WestPort
    return reward_port
end
merge_states(s1::Symbol, s2::Symbol) = Symbol(String(s1) * " - " * String(s2))
function extract_main_state(merged_state::Symbol, index::Int = 1)::Symbol
    splits = split(String(merged_state), " - ")
    if index <= length(splits) && index > 0
        state = splits[index]
    else
        error("Impossible state index $index")
    end
    return Symbol(state)
end
function flatten_states(
    states1::Union{Set{Symbol},OrderedSet{Symbol}},
    states2::Union{Set{Symbol},OrderedSet{Symbol}},
)::OrderedSet{Symbol}
    flattened_states = OrderedSet()
    for s1 ∈ states1
        for s2 ∈ states2
            push!(flattened_states, merge_states(s1, s2))
        end
    end
    return flattened_states
end

# Setup the environment
const StartOfEpisode = :Arena
Base.@kwdef mutable struct DiamondArenaMdp <: AbstractEnv
    current_state::Symbol = merge_states(StartOfEpisode, :NoOdor)
    cue_port::Symbol = initialize_episode().cue_port
    odor_ID::Symbol = initialize_episode().odor_ID
    reward_port::Symbol = get_reward_port(cue_port, odor_ID)
    reward::Float64 = 0.0
    odor_cond::Symbol = :pre
end

const DO_NOTHING_NEG_REWARD = -1.0
const NO_REWARD = 0.0
const SMALL_REWARD = 1.0
const BIG_REWARD = 10.0

function get_mdp(cue_port::Symbol, odor_cond::Symbol, odor_ID::Symbol)::NamedTuple
    reward_port::Symbol = get_reward_port(cue_port, odor_ID)
    mdp = (
        Arena = (
            DoNothing = (next_state = :Arena, reward = DO_NOTHING_NEG_REWARD),
            MoveNorth = (next_state = :NorthPort, reward = DO_NOTHING_NEG_REWARD),
            MoveSouth = (next_state = :SouthPort, reward = DO_NOTHING_NEG_REWARD),
            MoveEast = (next_state = :EastPort, reward = DO_NOTHING_NEG_REWARD),
            MoveWest = (next_state = :WestPort, reward = DO_NOTHING_NEG_REWARD),
            MoveArena = (next_state = :Arena, reward = DO_NOTHING_NEG_REWARD),
        ),
    )
    if odor_cond == :pre
        mdp = merge(
            mdp,
            (
                NorthPort = merge(
                    (
                        DoNothing = (
                            next_state = :NorthPort,
                            reward = DO_NOTHING_NEG_REWARD,
                        ),
                        MoveArena = (next_state = :Arena, reward = DO_NOTHING_NEG_REWARD),
                    ),
                    (
                        cue_port == :NorthPort ?
                        (PokePlusSniff = (next_state = cue_port, reward = SMALL_REWARD),) : NamedTuple()
                    ),
                ),
                SouthPort = merge(
                    (
                        DoNothing = (
                            next_state = :SouthPort,
                            reward = DO_NOTHING_NEG_REWARD,
                        ),
                        MoveArena = (next_state = :Arena, reward = DO_NOTHING_NEG_REWARD),
                    ),
                    (
                        cue_port == :SouthPort ?
                        (PokePlusSniff = (next_state = cue_port, reward = SMALL_REWARD),) : NamedTuple()
                    ),
                ),
                EastPort = (
                    DoNothing = (next_state = :EastPort, reward = DO_NOTHING_NEG_REWARD),
                    MoveArena = (next_state = :Arena, reward = DO_NOTHING_NEG_REWARD),
                ),
                WestPort = (
                    DoNothing = (next_state = :WestPort, reward = DO_NOTHING_NEG_REWARD),
                    MoveArena = (next_state = :Arena, reward = DO_NOTHING_NEG_REWARD),
                ),
            ),
        )
    elseif odor_cond == :post
        mdp = merge(
            mdp,
            (
                NorthPort = (
                    DoNothing = (next_state = :NorthPort, reward = DO_NOTHING_NEG_REWARD),
                    MoveArena = (next_state = :Arena, reward = DO_NOTHING_NEG_REWARD),
                ),
                SouthPort = (
                    DoNothing = (next_state = :SouthPort, reward = DO_NOTHING_NEG_REWARD),
                    MoveArena = (next_state = :Arena, reward = DO_NOTHING_NEG_REWARD),
                ),
                EastPort = merge(
                    (
                        DoNothing = (
                            next_state = :EastPort,
                            reward = DO_NOTHING_NEG_REWARD,
                        ),
                        MoveArena = (next_state = :Arena, reward = DO_NOTHING_NEG_REWARD),
                    ),
                    (
                        reward_port == :EastPort ?
                        (
                            PokePlusLick = (
                                next_state = :EndOfEpisode,
                                reward = BIG_REWARD,
                            ),
                        ) :
                        (Poke = (next_state = :EndOfEpisode, reward = -BIG_REWARD),)
                    ),
                ),
                WestPort = merge(
                    (
                        DoNothing = (
                            next_state = :WestPort,
                            reward = DO_NOTHING_NEG_REWARD,
                        ),
                        MoveArena = (next_state = :Arena, reward = DO_NOTHING_NEG_REWARD),
                    ),
                    (
                        reward_port == :WestPort ?
                        (
                            PokePlusLick = (
                                next_state = :EndOfEpisode,
                                reward = BIG_REWARD,
                            ),
                        ) :
                        (Poke = (next_state = :EndOfEpisode, reward = -BIG_REWARD),)
                    ),
                ),
                EndOfEpisode = (
                    Restart = (next_state = StartOfEpisode, reward = NO_REWARD),
                ),
            ),
        )
    end
    # pprint(mdp)
    return mdp
end

# Minimal Interfaces to Implement
RLBase.action_space(env::DiamondArenaMdp) = collect(Actions)
RLBase.reward(env::DiamondArenaMdp) = env.reward
RLBase.state(env::DiamondArenaMdp) = env.current_state

# Note that actions and states should be presented to `TabularApproximator` as integers
# starting from 1 to be used as the index of the table. That is, e.g., is expected to
# return `Base.OneTo(n_state)`, where `n_state` is the number of states.
# For `table` of 2-d, the first dimension is action and the second dimension is state.
RLBase.state_space(env::DiamondArenaMdp) = flatten_states(States, OdorID)
RLBase.is_terminated(env::DiamondArenaMdp) = extract_main_state(state(env)) == :EndOfEpisode
function RLBase.reset!(env::DiamondArenaMdp)
    env.current_state = merge_states(StartOfEpisode, :NoOdor)
    env.reward = 0.0
    env.cue_port, env.odor_ID = initialize_episode()
    env.reward_port = get_reward_port(env.cue_port, env.odor_ID)
    env.odor_cond = :pre

    with_logger(logger) do
        @debug("-----------------------------------")
        @debug("Reset environment")
        @debug("Cue port: $(env.cue_port)")
        @debug("Reward port: $(env.reward_port)")
        @debug("Odor condition: $(env.odor_cond)")
        @debug("Odor ID: $(env.odor_ID)")
        @debug("New state: $(env.current_state)")
        @debug("Reward: $(env.reward)")
        @debug("-----------------------------------")
    end
    return nothing
end
RLBase.ChanceStyle(env::DiamondArenaMdp) = RLBase.DETERMINISTIC
RLBase.ActionStyle(env::DiamondArenaMdp) = FULL_ACTION_SET
RLBase.InformationStyle(env::DiamondArenaMdp) = PERFECT_INFORMATION
RLBase.legal_action_space(env::DiamondArenaMdp) =
    RLBase.action_space(env)[legal_action_space_mask(env)]
function RLBase.legal_action_space_mask(env::DiamondArenaMdp)
    mdp = get_mdp(env.cue_port, env.odor_cond, env.odor_ID)
    mask = fill(false, size(RLBase.action_space(env)))
    allowed_actions = [item for item in keys(mdp[extract_main_state(state(env))])]
    ActionSpace = [Symbol(item) for item ∈ action_space(env)]
    for action ∈ allowed_actions
        mask = mask .|| (ActionSpace .== action)
    end
    mask
end

function get_odor(env::DiamondArenaMdp)::Symbol
    if env.odor_cond == :pre
        odor = :NoOdor
    elseif env.odor_cond == :post
        odor = env.odor_ID
    else
        error("Odor condition not possible")
    end
    return odor
end

"Environment logic"
function (env::DiamondArenaMdp)(action::Symbol)
    with_logger(logger) do
        @debug("-----------------------------------")
        @debug("Environment logic: before choosing action")
        @debug("Cue port: $(env.cue_port)")
        @debug("Reward port: $(env.reward_port)")
        @debug("Odor condition: $(env.odor_cond)")
        @debug("Odor ID: $(env.odor_ID)")
        @debug("Action: $action")
        @debug("Reward: $(env.reward)")
        @debug("-----------------------------------")
    end

    mdp = get_mdp(env.cue_port, env.odor_cond, env.odor_ID)
    original_state = state(env)  # Needs to store the state otherwise it gets overwriten

    # Important: the reward here corresponds to the reward the agent gets at the *current* step
    # *It shouldn't be incremented* as rewards collected by the hooks get already summed up
    env.reward = mdp[extract_main_state(original_state)][action][:reward]

    if action == :PokePlusSniff
        env.odor_cond = :post
        with_logger(logger) do
            @debug("PokePlusSniff condition")
        end
    end
    env.current_state = merge_states(
        mdp[extract_main_state(original_state)][action][:next_state],
        get_odor(env),
    )

    with_logger(logger) do
        @debug("-----------------------------------")
        @debug("Environment logic: after choosing action")
        @debug("Cue port: $(env.cue_port)")
        @debug("Reward port: $(env.reward_port)")
        @debug("Odor condition: $(env.odor_cond)")
        @debug("Odor ID: $(env.odor_ID)")
        @debug("Original state: $original_state")
        @debug("Action: $action")
        @debug("New state: $(env.current_state)")
        @debug("Reward: $(env.reward)")
        @debug("-----------------------------------")
    end
    return nothing
end

DiamondArenaEnv = DiamondArenaMdp()
# RLBase.test_runnable!(DiamondArenaEnv)

DiamondArenaWrapped = ActionTransformedEnv(
    StateTransformedEnv(
        DiamondArenaEnv;
        state_mapping = s -> begin
            res = findfirst(x -> x == s, state_space(DiamondArenaEnv))
            with_logger(logger) do
                @logmsg Logging.Debug "Space mapping" s res
            end
            res
        end,
        state_space_mapping = _ -> Base.OneTo(length(state_space(DiamondArenaEnv))),
    );
    action_mapping = a -> begin
        res = action_space(DiamondArenaEnv)[a]
        with_logger(logger) do
            @logmsg Logging.Debug "Action mapping" a res
        end
        res
    end,
    action_space_mapping = _ -> Base.OneTo(length(action_space(DiamondArenaEnv))),
)

NS = length(state_space(DiamondArenaEnv))
NA = length(action_space(DiamondArenaEnv))


"4 distinct ports + odor environment"
function four_distinct_ports_odor_env()
    return DiamondArenaWrapped, NS, NA
end
