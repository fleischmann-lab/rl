
"Custom hook to record what happens during the episode"
Base.@kwdef mutable struct EpisodeBehavior <: AbstractHook
    steps::Vector{Int} = Int[]
    visitN::Vector{Int} = Int[]
    visitS::Vector{Int} = Int[]
    visitE::Vector{Int} = Int[]
    visitW::Vector{Int} = Int[]
    rewards::Vector{Float64} = Float64[]
    startEpisode::Vector{Int} = Int[]
    endEpisode::Vector{Int} = Int[]
end
function (hook::EpisodeBehavior)(::PreEpisodeStage, agent, env)
    if length(hook.steps) == 0
        push!(hook.steps, 1)
    else
        push!(hook.steps, hook.steps[end] + 1)
    end
    state(env) == 2 ? push!(hook.visitN, 1) : push!(hook.visitN, 0)
    state(env) == 3 ? push!(hook.visitS, 1) : push!(hook.visitS, 0)
    state(env) == 4 ? push!(hook.visitE, 1) : push!(hook.visitE, 0)
    state(env) == 5 ? push!(hook.visitW, 1) : push!(hook.visitW, 0)
    push!(hook.rewards, reward(env))
    push!(hook.startEpisode, 1)
    push!(hook.endEpisode, 0)
end
function (hook::EpisodeBehavior)(::PostActStage, agent, env)
    push!(hook.steps, hook.steps[end] + 1)
    state(env) == 2 ? push!(hook.visitN, 1) : push!(hook.visitN, 0)
    state(env) == 3 ? push!(hook.visitS, 1) : push!(hook.visitS, 0)
    state(env) == 4 ? push!(hook.visitE, 1) : push!(hook.visitE, 0)
    state(env) == 5 ? push!(hook.visitW, 1) : push!(hook.visitW, 0)
    push!(hook.rewards, reward(env))
    push!(hook.startEpisode, 0)
    push!(hook.endEpisode, 0)
end
function (hook::EpisodeBehavior)(::PostEpisodeStage, agent, env)
    push!(hook.steps, hook.steps[end])
    state(env) == 2 ? push!(hook.visitN, 1) : push!(hook.visitN, 0)
    state(env) == 3 ? push!(hook.visitS, 1) : push!(hook.visitS, 0)
    state(env) == 4 ? push!(hook.visitE, 1) : push!(hook.visitE, 0)
    state(env) == 5 ? push!(hook.visitW, 1) : push!(hook.visitW, 0)
    push!(hook.rewards, reward(env))
    push!(hook.startEpisode, 0)
    push!(hook.endEpisode, 1)
end

"Custom hook to record what happens during the episode"
Base.@kwdef mutable struct EpisodeBehaviorOdorObs <: AbstractHook
    steps::Vector{Int} = Int[]
    visitN::Vector{Int} = Int[]
    visitS::Vector{Int} = Int[]
    visitE::Vector{Int} = Int[]
    visitW::Vector{Int} = Int[]
    rewards::Vector{Float64} = Float64[]
    startEpisode::Vector{Int} = Int[]
    endEpisode::Vector{Int} = Int[]
end
function (hook::EpisodeBehaviorOdorObs)(::PreEpisodeStage, agent, env)
    if length(hook.steps) == 0
        push!(hook.steps, 1)
    else
        push!(hook.steps, hook.steps[end] + 1)
    end
    state(env) ∈ [4, 5, 6] ? push!(hook.visitN, 1) : push!(hook.visitN, 0)
    state(env) ∈ [7, 8, 9] ? push!(hook.visitS, 1) : push!(hook.visitS, 0)
    state(env) ∈ [10, 11, 12] ? push!(hook.visitE, 1) : push!(hook.visitE, 0)
    state(env) ∈ [13, 14, 15] ? push!(hook.visitW, 1) : push!(hook.visitW, 0)
    push!(hook.rewards, reward(env))
    push!(hook.startEpisode, 1)
    push!(hook.endEpisode, 0)
end
function (hook::EpisodeBehaviorOdorObs)(::PostActStage, agent, env)
    push!(hook.steps, hook.steps[end] + 1)
    state(env) ∈ [4, 5, 6] ? push!(hook.visitN, 1) : push!(hook.visitN, 0)
    state(env) ∈ [7, 8, 9] ? push!(hook.visitS, 1) : push!(hook.visitS, 0)
    state(env) ∈ [10, 11, 12] ? push!(hook.visitE, 1) : push!(hook.visitE, 0)
    state(env) ∈ [13, 14, 15] ? push!(hook.visitW, 1) : push!(hook.visitW, 0)
    push!(hook.rewards, reward(env))
    push!(hook.startEpisode, 0)
    push!(hook.endEpisode, 0)
end
function (hook::EpisodeBehaviorOdorObs)(::PostEpisodeStage, agent, env)
    push!(hook.steps, hook.steps[end])
    state(env) ∈ [4, 5, 6] ? push!(hook.visitN, 1) : push!(hook.visitN, 0)
    state(env) ∈ [7, 8, 9] ? push!(hook.visitS, 1) : push!(hook.visitS, 0)
    state(env) ∈ [10, 11, 12] ? push!(hook.visitE, 1) : push!(hook.visitE, 0)
    state(env) ∈ [13, 14, 15] ? push!(hook.visitW, 1) : push!(hook.visitW, 0)
    push!(hook.rewards, reward(env))
    push!(hook.startEpisode, 0)
    push!(hook.endEpisode, 1)
end
