using LoggingExtras
using Dates


"Common logging settings"
function logging(logging_level)
    date_format = "yyyy-mm-dd HH:MM:SS"
    logfile = datadir("logs.txt")
    io = open(logfile, "w")
    timestamp_logger(logger) =
        TransformerLogger(logger) do log
            merge(log, (; message = "$(Dates.format(now(), date_format)) - $(log.message)"))
        end
    logger = Base.SimpleLogger(io, logging_level) |> timestamp_logger
    return io, logger
end

"Postprocess CSV file with behavioral data"
function EpisodeBehaviorOdorObsPostProcess(csvpath)
    df = CSV.File(csvpath) |> DataFrame

    # Give a nuber to each port for plotting
    df.S = df.S * 2
    df.E = df.E * 3
    df.W = df.W * 4

    # Get the ports as a chronological sequence
    df.sequence = df.N .| df.S .| df.E .| df.W

    # Get the port of the rewarded/correct episodes
    diffRe = prepend!(diff(circshift(df.Re, -1)), 0)
    df.RePort = circshift(df.stop, -2)
    df.RePort[df.RePort.>0] = df.E[df.RePort.>0] .| df.W[df.RePort.>0]
    df.RePort[diffRe.≠10] .= 0

    # Get the port starting the episode
    df.StartPort = zeros(length(df.start))
    start_idx = findall(df.start .> 0)
    for idx ∈ start_idx
        nextN = findnext(df.N .> 0, idx)
        nextS = findnext(df.S .> 0, idx)

        # Workaround for bug or strange behavior as
        # the environment seems to be restarted a few times at the end
        nextidx = filter!(x -> x != nothing, [nextN, nextS])
        if length(nextidx) == 2
            idx_port = min(nextN, nextS)
        elseif length(nextidx) == 1
            idx_port = nextidx[1]
        end
        if @isdefined idx_port
            df.StartPort[idx_port] = df.sequence[idx_port]
        end
    end
    return df
end
