using ReinforcementLearning
# using PrettyPrinting


@enum OdorCondition pre post
@enum CuePorts North South
@enum RewardPorts East West
@enum States Arena NorthPort SouthPort EastPort WestPort Odor EndOfEpisode
@enum Actions begin
    DoNothing
    MoveNorth
    MoveSouth
    MoveEast
    MoveWest
    MoveArena
    PokePlusSniff
    PokePlusLick
    Poke
    Restart
end

function initialize_cue_port()
    cue_port = sample(collect(instances(CuePorts)))
    return cue_port
end
function get_reward_port(cue_port::CuePorts)
    if cue_port == North
        reward_port = East
    elseif cue_port == South
        reward_port = West
    end
    return reward_port
end

# Setup the environment
StartOfEpisode = Arena
Base.@kwdef mutable struct DiamondArenaMdp <: AbstractEnv
    current_state::States = StartOfEpisode
    cue_port::CuePorts = initialize_cue_port()
    reward_port::RewardPorts = get_reward_port(cue_port)
    reward::Float64 = 0.0
    odor_cond::OdorCondition = pre
end

DO_NOTHING_NEG_REWARD = -1.0
NO_REWARD = 0.0
SMALL_REWARD = 1.0
BIG_REWARD = 10.0

function get_mdp(cue_port::CuePorts, odor_cond::OdorCondition)
    reward_port::RewardPorts = get_reward_port(cue_port)
    mdp = (
        Arena = (
            DoNothing = (next_state = Arena, reward = DO_NOTHING_NEG_REWARD),
            MoveNorth = (next_state = NorthPort, reward = NO_REWARD),
            MoveSouth = (next_state = SouthPort, reward = NO_REWARD),
            MoveEast = (next_state = EastPort, reward = NO_REWARD),
            MoveWest = (next_state = WestPort, reward = NO_REWARD),
            MoveArena = (next_state = Arena, reward = DO_NOTHING_NEG_REWARD),
        ),
    )
    if odor_cond == pre
        mdp = merge(
            mdp,
            (
                NorthPort = merge(
                    (
                        DoNothing = (
                            next_state = NorthPort,
                            reward = DO_NOTHING_NEG_REWARD,
                        ),
                        MoveArena = (next_state = Arena, reward = DO_NOTHING_NEG_REWARD),
                    ),
                    (
                        cue_port == North ?
                        (PokePlusSniff = (next_state = Odor, reward = SMALL_REWARD),) :
                        NamedTuple()
                    ),
                ),
                SouthPort = merge(
                    (
                        DoNothing = (
                            next_state = SouthPort,
                            reward = DO_NOTHING_NEG_REWARD,
                        ),
                        MoveArena = (next_state = Arena, reward = DO_NOTHING_NEG_REWARD),
                    ),
                    (
                        cue_port == South ?
                        (PokePlusSniff = (next_state = Odor, reward = SMALL_REWARD),) :
                        NamedTuple()
                    ),
                ),
                EastPort = (
                    DoNothing = (next_state = EastPort, reward = DO_NOTHING_NEG_REWARD),
                    MoveArena = (next_state = Arena, reward = DO_NOTHING_NEG_REWARD),
                ),
                WestPort = (
                    DoNothing = (next_state = WestPort, reward = DO_NOTHING_NEG_REWARD),
                    MoveArena = (next_state = Arena, reward = DO_NOTHING_NEG_REWARD),
                ),
                Odor = (
                    DoNothing = (next_state = Odor, reward = DO_NOTHING_NEG_REWARD),
                    MoveArena = (next_state = Arena, reward = DO_NOTHING_NEG_REWARD),
                ),
            ),
        )
    elseif odor_cond == post
        mdp = merge(
            mdp,
            (
                NorthPort = (
                    DoNothing = (next_state = NorthPort, reward = DO_NOTHING_NEG_REWARD),
                    MoveArena = (next_state = Arena, reward = DO_NOTHING_NEG_REWARD),
                ),
                SouthPort = (
                    DoNothing = (next_state = SouthPort, reward = DO_NOTHING_NEG_REWARD),
                    MoveArena = (next_state = Arena, reward = DO_NOTHING_NEG_REWARD),
                ),
                EastPort = merge(
                    (
                        DoNothing = (next_state = EastPort, reward = DO_NOTHING_NEG_REWARD),
                        MoveArena = (next_state = Arena, reward = DO_NOTHING_NEG_REWARD),
                    ),
                    (
                        reward_port == East ?
                        (PokePlusLick = (next_state = EndOfEpisode, reward = BIG_REWARD),) : (Poke = (next_state = EndOfEpisode, reward = -BIG_REWARD),)
                    ),
                ),
                WestPort = merge(
                    (
                        DoNothing = (next_state = WestPort, reward = DO_NOTHING_NEG_REWARD),
                        MoveArena = (next_state = Arena, reward = DO_NOTHING_NEG_REWARD),
                    ),
                    (
                        reward_port == West ?
                        (PokePlusLick = (next_state = EndOfEpisode, reward = BIG_REWARD),) : (Poke = (next_state = EndOfEpisode, reward = -BIG_REWARD),)
                    ),
                ),
                Odor = (
                    DoNothing = (next_state = Odor, reward = DO_NOTHING_NEG_REWARD),
                    MoveArena = (next_state = Arena, reward = DO_NOTHING_NEG_REWARD),
                ),
                EndOfEpisode = (
                    Restart = (next_state = StartOfEpisode, reward = NO_REWARD),
                ),
            ),
        )
    end
    # pprint(mdp)
    return mdp
end

# Minimal Interfaces to Implement
RLBase.action_space(env::DiamondArenaMdp) = instances(Actions)
RLBase.reward(env::DiamondArenaMdp) = env.reward
RLBase.state(env::DiamondArenaMdp) = env.current_state
RLBase.state_space(env::DiamondArenaMdp) = instances(States)
RLBase.is_terminated(env::DiamondArenaMdp) = state(env) == EndOfEpisode
function RLBase.reset!(env::DiamondArenaMdp)
    env.current_state = StartOfEpisode
    env.reward = 0.0
    env.cue_port = initialize_cue_port()
    env.reward_port = get_reward_port(env.cue_port)
    env.odor_cond = pre
    nothing
end
RLBase.ChanceStyle(env::DiamondArenaMdp) = RLBase.DETERMINISTIC
RLBase.ActionStyle(env::DiamondArenaMdp) = FULL_ACTION_SET
RLBase.InformationStyle(env::DiamondArenaMdp) = PERFECT_INFORMATION
RLBase.legal_action_space(env::DiamondArenaMdp) =
    RLBase.action_space(env)[legal_action_space_mask(env)]
function RLBase.legal_action_space_mask(env::DiamondArenaMdp)
    mdp = get_mdp(env.cue_port, env.odor_cond)
    mask = fill(false, size(collect(RLBase.action_space(env))))
    allowed_actions = [item for item in keys(mdp[Symbol(state(env))])]
    ActionSpace = [Symbol(item) for item ∈ action_space(env)]
    for action ∈ allowed_actions
        mask = mask .|| (ActionSpace .== action)
    end
    mask
end

"Environment logic"
function (env::DiamondArenaMdp)(action::Actions)
    mdp = get_mdp(env.cue_port, env.odor_cond)
    original_state = state(env)  # Needs to store the state otherwise it gets overwriten
    env.current_state = mdp[Symbol(original_state)][Symbol(action)][:next_state]
    env.reward += mdp[Symbol(original_state)][Symbol(action)][:reward]
    if env.current_state == Odor
        env.odor_cond = post
    end

    with_logger(logger) do
        @debug("-------------------------------")
        @debug("Cue port: $(env.cue_port)")
        @debug("Reward port: $(env.reward_port)")
        @debug("Odor condition: $(env.odor_cond)")
        @debug("Original state: $original_state")
        @debug("Action: $action")
        @debug("New state: $(env.current_state)")
        @debug("Reward: $(env.reward)")
        @debug("-------------------------------")
    end
    nothing
end

DiamondArenaEnv = DiamondArenaMdp()
# RLBase.test_runnable!(DiamondArenaMdp())

DiamondArenaWrapped = ActionTransformedEnv(
    StateTransformedEnv(
        DiamondArenaEnv;
        state_mapping = s -> begin
            res = findfirst(x -> x == s, state_space(DiamondArenaEnv))
            with_logger(logger) do
                @logmsg Logging.Debug "Space mapping" s res
            end
            res
        end,
        state_space_mapping = _ -> Base.OneTo(length(state_space(DiamondArenaEnv))),
    );
    action_mapping = a -> begin
        res = action_space(DiamondArenaEnv)[a]
        with_logger(logger) do
            @logmsg Logging.Debug "Action mapping" a res
        end
        res
    end,
    action_space_mapping = _ -> Base.OneTo(length(action_space(DiamondArenaEnv))),
)

NS, NA = length(state_space(DiamondArenaEnv)), length(action_space(DiamondArenaEnv))


"4 distinct port environment"
function four_distinct_ports_env()
    return DiamondArenaWrapped, NS, NA
end
